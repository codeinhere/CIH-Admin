<?php

namespace common\services;

use Yii;

class verifyCodeClass
{

    private $charset = 'abcdefghkmnprstuvwxyzABCDEFGHKMNPRSTUVWXYZ23456789'; //随机因子

    private $code; //验证码

    private $codeLength = 4; //验证码长度

    private $width = 130; //宽度

    private $height = 50; //高度

    private $img; //图形资源句柄

    private $font; //指定的字体

    private $fontSize = 30; //指定字体大小

    private $fontColor; //指定字体颜色

    /**
     * redis 键
     */
    const REDIS_KEY_PREFIX = 'VERIFY_CODE_';

    /**
     * 构造函数
     * verifyCodeClass constructor.
     */
    public function __construct()
    {
        $path = Yii::getAlias( '@webroot' ) . '/fonts/elephant.ttf';
        $this->font = $path;
    }

    /**
     * 生成随机码
     */
    private function createCode()
    {
        $_len = strlen( $this->charset ) - 1;
        for ( $i = 0 ; $i < $this->codeLength ; $i++ ) {
            $this->code .= $this->charset[ mt_rand( 0 , $_len ) ];
        }
    }

    /**
     * 生成背景
     */
    private function createBg()
    {
        $this->img = imagecreatetruecolor( $this->width , $this->height );
        $color = imagecolorallocate( $this->img , mt_rand( 157 , 255 ) , mt_rand( 157 , 255 ) , mt_rand( 157 , 255 ) );
        imagefilledrectangle( $this->img , 0 , $this->height , $this->width , 0 , $color );
    }

    /**
     * 生成文字
     */
    private function createFont()
    {
        $_x = $this->width / $this->codeLength;
        for ( $i = 0 ; $i < $this->codeLength ; $i++ ) {
            $this->fontColor = imagecolorallocate( $this->img , mt_rand( 0 , 156 ) , mt_rand( 0 , 156 ) , mt_rand( 0 , 156 ) );
            imagettftext( $this->img , $this->fontSize , mt_rand( -30 , 30 ) , $_x * $i + mt_rand( 1 , 5 ) , $this->height / 1.4 , $this->fontColor , $this->font , $this->code[ $i ] );
        }
    }

    /**
     * 生成线条、雪花
     */
    private function createLine()
    {
        //线条
        for ( $i = 0 ; $i < 6 ; $i++ ) {
            $color = imagecolorallocate( $this->img , mt_rand( 0 , 156 ) , mt_rand( 0 , 156 ) , mt_rand( 0 , 156 ) );
            imageline( $this->img , mt_rand( 0 , $this->width ) , mt_rand( 0 , $this->height ) , mt_rand( 0 , $this->width ) , mt_rand( 0 , $this->height ) , $color );
        }
        //雪花
        for ( $i = 0 ; $i < 100 ; $i++ ) {
            $color = imagecolorallocate( $this->img , mt_rand( 200 , 255 ) , mt_rand( 200 , 255 ) , mt_rand( 200 , 255 ) );
            imagestring( $this->img , mt_rand( 1 , 5 ) , mt_rand( 0 , $this->width ) , mt_rand( 0 , $this->height ) , '*' , $color );
        }
    }

    //输出

    /**
     * 输出验证码图片
     * @return string
     */
    private function outPut()
    {
        $str = time() + rand( 0 , 99999 );
        $md5Str = md5( $str );
        $imageName = substr( $md5Str , 0 , 8 ) . '.png';
        imagepng( $this->img , Yii::getAlias( '@webroot' ) . '/' . Yii::$app->params[ 'verifyCodeImageDir' ] . '/' . $imageName );
        return $imageName;
    }

    /**
     * 对外生成
     * @return string
     */
    public function getImage()
    {
        $this->createBg();
        $this->createCode();
        $this->createLine();
        $this->createFont();
        return $this->outPut();
    }

    //获取验证码
    public function getCode()
    {
        return strtolower( $this->code );
    }
}

