<?php

namespace common\models;

use Yii;
/*ar*/
use common\ActiveRecord\BackendAdmin;
use common\ActiveRecord\RbacAdminRole;
use common\ActiveRecord\RbacPermission;
use common\ActiveRecord\RbacPermissionRole;
use common\ActiveRecord\RbacRole;
use common\ActiveRecord\RbacMenu;
/*model*/
use common\models\BackendAdminModel;

/**
 * 处理权限数据模型
 * Class PermissionModel
 * @package common\models
 */
class iPermissionModel
{

    /**
     * 查询管理员数据
     *
     * @param $offset
     * @param $limit
     *
     * @return array
     */
    public static function getAdmins( $offset , $limit )
    {
        $adminUsername = Yii::$app->request->queryParams[ 'admin_username' ] ?? null;
        $status = Yii::$app->request->queryParams[ 'status' ] ?? null;
        $createBeginTime = Yii::$app->request->queryParams[ 'create-begin-time' ] ?? null;
        if ( $createBeginTime != null ) {
            $createBeginTime = strtotime( $createBeginTime );
        }
        $createEndTime = Yii::$app->request->queryParams[ 'create-end-time' ] ?? null;
        if ( $createEndTime != null ) {
            $createEndTime = strtotime( $createEndTime );
        }
        $query = BackendAdmin::find()
            ->orderBy( [ 'register_time' => SORT_DESC ] )
            //->andWhere( [ '<>' , 'status' , BackendAdminModel::STATUS_DELETE ] )
            ->andFilterWhere( [ 'like' , 'username' , $adminUsername ] )
            ->andFilterWhere( [ '=' , 'status' , $status ] )
            ->andFilterWhere( [ '>=' , 'register_time' , $createBeginTime ] )
            ->andFilterWhere( [ '<=' , 'register_time' , $createEndTime ] );
        $data = $query->offset( $offset )->limit( $limit )->all();
        $count = $query->count();
        $pages = ceil( $count / $limit );
        $currentPage = $count / $limit - ( $count - $offset ) / $limit + 1;
        return [
            'data' => $data ,
            'pages' => $pages ,
            'count' => $count ,
            'currentPage' => $currentPage ,
        ];
    }

    /**
     * 查询所有角色数据
     *
     * @param $offset
     * @param $limit
     *
     * @return array
     */
    public static function getRoles( $offset , $limit )
    {
        $roleName = urldecode( Yii::$app->request->queryParams[ 'role_name' ] ?? null );
        $status = urldecode( Yii::$app->request->queryParams[ 'status' ] ?? null );
        $createBeginTime = urldecode( Yii::$app->request->queryParams[ 'cerate-begin-time' ] ?? null );
        if ( $createBeginTime != null ) {
            $createBeginTime = strtotime( $createBeginTime );
        }
        $createEndTime = urldecode( Yii::$app->request->queryParams[ 'create-end-time' ] ?? null );
        if ( $createEndTime != null ) {
            $createEndTime = strtotime( $createEndTime );
        }
        $query = RbacRole::find()
            //->where( [ '<>' , 'status' , RbacRoleModel::STATUS_DELETE ] )
            ->orderBy( [ 'create_time' => SORT_DESC ] )
            ->andFilterWhere( [ 'like' , 'role_name' , $roleName ] )
            ->andFilterWhere( [ '=' , 'status' , $status ] )
            ->andFilterWhere( [ '>=' , 'create_time' , $createBeginTime ] )
            ->andFilterWhere( [ '<=' , 'create_time' , $createEndTime ] );
        $data = $query->offset( $offset )
            ->limit( $limit )
            ->all();
        $count = $query->count();
        $pages = ceil( $count / $limit );
        $currentPage = $count / $limit - ( $count - $offset ) / $limit + 1;
        return [
            'data' => $data ,
            'pages' => $pages ,
            'count' => $count ,
            'currentPage' => $currentPage ,
        ];
    }

    /**
     * 获取所有权限
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPermissions()
    {
        /*
        $permissionName = urldecode( Yii::$app->request->queryParams[ 'permission_name' ] ?? '' );
        $controllerName = urldecode( Yii::$app->request->queryParams[ 'controller_name' ] ?? '' );
        $actionName = urldecode( Yii::$app->request->queryParams[ 'action_name' ] ?? '' );
        */
        $permissions = RbacPermission::find()
            ->with( 'childPermissions' )
            ->where( [ '=' , 'parent_id' , 0 ] )
            //->andFilterWhere( [ 'like' , 'permission_name' , $permissionName ] )
            //->orderBy( [ 'permission_name' => SORT_DESC ] )
            ->all();
        if ( !empty( $controllerName ) || !empty( $actionName ) ) {
            foreach ( $permissions as $k => $v ) {
                if ( empty( $v[ 'childPermissions' ] ) ) {
                    unset( $permissions[ $k ] );
                }
            }
        }
        /*
        foreach ( $permissions as $k => $v ) {
            if ( empty( $v[ 'childPermissions' ] ) ) {
                unset( $permissions[ $k ] );
            }
        }
        */
        return $permissions;
    }

    /**
     * 根据角色获取分配的权限
     *
     * @param null $roleId
     *
     * @return array
     */
    public static function getAssignedPermissions( $roleId = null )
    {
        //获取所有权限
        $permissions = RbacPermission::find()
            ->with( 'childPermissions' )
            ->where( [ '=' , 'parent_id' , 0 ] )
            //->andWhere( [ '=' , 'status' , RbacPermissionModel::STATUS_ENABLE ] )
            ->all();
        //获取角色拥有的权限
        $assignedPermissions = RbacPermissionRole::findAll( [ 'role_id' => $roleId ] );
        $iAssignedPermissions = [];
        foreach ( $assignedPermissions as $k => $v ) {
            $iAssignedPermissions[] = $v[ 'permission_id' ];
        }
        $iPermissions = [];
        //对比权限并标示管理员是否有权限
        foreach ( $permissions as $permission ) {
            $tmp = [];
            $tmp[ 'id' ] = $permission[ 'id' ];
            $tmp[ 'permission_name' ] = $permission[ 'permission_name' ];
            $tmp[ 'route_controller' ] = $permission[ 'route_controller' ];
            $tmp[ 'route_action' ] = $permission[ 'route_action' ];
            $tmp[ 'parent_id' ] = $permission[ 'parent_id' ];
            $tmp[ 'status' ] = $permission[ 'status' ];
            if ( in_array( $tmp[ 'id' ] , $iAssignedPermissions ) ) {
                $tmp[ 'has_permission' ] = 'true';
            } else {
                $tmp[ 'has_permission' ] = 'false';
            }
            foreach ( $permission[ 'childPermissions' ] as $childPermission ) {
                $_tmp = [];
                $_tmp[ 'id' ] = $childPermission[ 'id' ];
                $_tmp[ 'permission_name' ] = $childPermission[ 'permission_name' ];
                $_tmp[ 'route_controller' ] = $childPermission[ 'route_controller' ];
                $_tmp[ 'route_action' ] = $childPermission[ 'route_action' ];
                $_tmp[ 'parent_id' ] = $childPermission[ 'parent_id' ];
                $_tmp[ 'status' ] = $childPermission[ 'status' ];
                if ( in_array( $_tmp[ 'id' ] , $iAssignedPermissions ) ) {
                    $_tmp[ 'has_permission' ] = 'true';
                } else {
                    $_tmp[ 'has_permission' ] = 'false';
                }
                $tmp[ 'childPermission' ][] = $_tmp;
            }
            $iPermissions[] = $tmp;
        }
        return $iPermissions;
    }
}


