<?php

namespace common\models;

use common\ActiveRecord\BackendAdmin;
use common\ActiveRecord\RbacAdminRole;
use Yii;

class BackendAdminModel
{

    /**
     * 账户状态：正常
     */
    const STATUS_ENABLE = 1;

    /**
     * 账户状态：禁用
     */
    const STATUS_DISABLE = 0;

    /**
     * 账户状态：删除
     */
    const STATUS_DELETE = 2;

    /**
     * 删除管理员
     *
     * @param $adminId
     *
     * @return bool
     */
    public static function deleteAdmin( $adminId )
    {
        $admin = BackendAdmin::findOne( $adminId );
        if ( !$admin || empty( $admin ) ) {
            return true;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            //修改管理员状态为删除
            BackendAdmin::updateAll( [ 'status' => self::STATUS_DELETE ] , [ 'id' => $adminId ] );
            //删除管理员的角色权限
            RbacAdminRole::deleteAll( [ 'admin_id' => $adminId ] );
            //todo 删除管理员权限
            $transaction->commit();
            return true;
        } catch ( \Exception $e ) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * 查询管理员数据
     *
     * @param $offset
     * @param $limit
     *
     * @return array
     */
    public static function getAdmins( $offset , $limit )
    {
        $adminUsername = Yii::$app->request->queryParams[ 'admin_username' ] ?? null;
        $status = Yii::$app->request->queryParams[ 'status' ] ?? null;
        $createBeginTime = Yii::$app->request->queryParams[ 'create-begin-time' ] ?? null;
        if ( $createBeginTime != null ) {
            $createBeginTime = strtotime( $createBeginTime );
        }
        $createEndTime = Yii::$app->request->queryParams[ 'create-end-time' ] ?? null;
        if ( $createEndTime != null ) {
            $createEndTime = strtotime( $createEndTime );
        }
        $query = BackendAdmin::find()
            ->orderBy( [ 'register_time' => SORT_DESC ] )
            ->where( [ '<>' , 'status' , self::STATUS_DELETE ] )
            ->andFilterWhere( [ 'like' , 'username' , $adminUsername ] )
            ->andFilterWhere( [ '=' , 'status' , $status ] )
            ->andFilterWhere( [ '>=' , 'register_time' , $createBeginTime ] )
            ->andFilterWhere( [ '<=' , 'register_time' , $createEndTime ] );
        $data = $query->offset( $offset )->limit( $limit )->all();
        $count = $query->count();
        $pages = ceil( $count / $limit );
        $currentPage = $count / $limit - ( $count - $offset ) / $limit + 1;
        return [
            'data' => $data ,
            'pages' => $pages ,
            'count' => $count ,
            'currentPage' => $currentPage ,
        ];
    }

    /**
     * 根据名称查询管理员
     *
     * @param $name
     * @param $limit
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchAdminByName( $name , $limit )
    {
        return BackendAdmin::find()
            ->andFilterWhere( [ 'like' , 'username' , $name ] )
            ->limit( $limit )
            ->all();
    }
}
