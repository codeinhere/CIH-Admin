<?php

namespace common\models;

use common\ActiveRecord\RbacPermission;

/**
 * 权限模型
 * Class RbacPermissionModel
 * @package common\models
 */
class RbacPermissionModel
{

    /**
     * 状态禁用
     */
    const STATUS_DISABLE = 0;

    /**
     * 状态启用
     */
    const STATUS_ENABLE = 1;

    /**
     * 父级权限id
     */
    const PARENT_ID = 0;

    /**
     * 获取所有权限
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPermissions()
    {
        $permissions = RbacPermission::find()
            ->with( 'childPermissions' )
            ->where( [ '=' , 'parent_id' , 0 ] )
            ->all();
        if ( !empty( $controllerName ) || !empty( $actionName ) ) {
            foreach ( $permissions as $k => $v ) {
                if ( empty( $v[ 'childPermissions' ] ) ) {
                    unset( $permissions[ $k ] );
                }
            }
        }
        return $permissions;
    }

    /**
     * 根据名称模糊查询权限
     *
     * @param $name
     * @param $limit
     *
     * @return mixed
     */
    public static function searchPermissionByName( $name , $limit )
    {
        return RbacPermission::find()
            ->where( [ '<>' , 'parent_id' , self::PARENT_ID ] )
            ->andFilterWhere( [ 'like' , 'permission_name' , $name ] )
            ->limit( $limit )
            ->all();
    }

    /**
     * 根据名称模糊查询父级权限
     *
     * @param $name
     * @param $limit
     *
     * @return mixed
     */
    public static function searchParentPermissionByName( $name , $limit )
    {
        return RbacPermission::find()
            ->where( [ '=' , 'parent_id' , self::PARENT_ID ] )
            ->andFilterWhere( [ 'like' , 'permission_name' , $name ] )
            ->limit( $limit )
            ->all();
    }

    /**
     * 获取父级权限
     * 关联子权限查询
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllParentPermissions()
    {
        return RbacPermission::find()
            ->with( [ 'childPermissions' ] )
            ->where( [ '=' , 'parent_id' , self::PARENT_ID ] )
            ->asArray()
            ->all();
    }
}
