<?php

namespace common\models;

use common\ActiveRecord\RbacMenu;

class RbacMenuModel
{

    /**
     * 默认父级 id 值
     */
    const PARENT_ID = 0;

    /**
     * 状态：可用
     */
    const STATUS_ENABLE = 1;

    /**
     * 状态：不可用
     */
    const STATUS_DISABLE = 0;

    /**
     * 可见
     */
    const VISIBLE_ENABLE = 1;

    /**
     * 不可见
     */
    const VISIBLE_DISABLE = 0;

    /**
     * 是父级菜单
     */
    const IS_PARENT = 1;

    /**
     * 不是父级菜单
     */
    const IS_NOT_PARENT = 0;

    /**
     * 根据名称模糊查询父级菜单
     *
     * @param $name
     * @param $limit
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchParentMenuByName( $name , $limit , $adminId )
    {
        return RbacMenu::find()
            ->where( [ '=' , 'parent_id' , 0 ] )
            ->andWhere( [ '=' , 'admin_id' , $adminId ] )
            ->andWhere( [ '=' , 'status' , self::STATUS_ENABLE ] )
            ->andWhere( [ '=' , 'visible' , self::VISIBLE_ENABLE ] )
            ->andFilterWhere( [ 'like' , 'menu_name' , $name ] )
            ->limit( $limit )
            ->all();
    }

    /**
     * 根据管理员 id 获取菜单
     *
     * @param $adminId
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMenusByAdminId( $adminId )
    {
        return RbacMenu::find()
            ->with( [ 'permission' , 'parent' ] )
            ->where( [ 'admin_id' => $adminId ] )
            ->all();
    }

    /**
     * 根据 id 和管理员 id 查询数据
     *
     * @param $id
     * @param $adminId
     *
     * @return null|static
     */
    public static function getMenuByIdAndAdminId( $id , $adminId )
    {
        return RbacMenu::findOne( [ 'id' => $id , 'admin_id' => $adminId ] );
    }

    /**
     * 根据 id 查询菜单数据
     *
     * @param $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getMenuById( $id )
    {
        return RbacMenu::find()
            ->with( [ 'permission' , 'parent' ] )
            ->where( [ 'id' => $id ] )
            ->one();
    }

    /**
     * 根据管理员 id 获取父级菜单
     *
     * @param $adminId
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getParentMenusByAdminId( $adminId )
    {
        return RbacMenu::find()
            ->orderBy( [ 'sort' => SORT_ASC ] )
            ->with( [ 'permission' , 'childMenus' ] )
            ->where( [ 'parent_id' => self::PARENT_ID ] )
            ->andWhere( [ 'admin_id' => $adminId ] )
            ->asArray()
            ->all();
    }

    /**
     * 获取左侧菜单
     *
     * @param $adminId
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getMenusTreeByAdminId( $adminId )
    {
        return RbacMenu::find()
            ->with( [ 'childMenusTree' , 'permission' ] )
            ->where( [ '=' , 'parent_id' , RbacMenuModel::PARENT_ID ] )
            ->andWhere( [ '=' , 'status' , RbacMenuModel::STATUS_ENABLE ] )
            ->andWhere( [ '=' , 'admin_id' , $adminId ] )
            ->andWhere( [ '=' , 'visible' , RbacMenuModel::VISIBLE_ENABLE ] )
            ->asArray()
            ->all();
    }
}
