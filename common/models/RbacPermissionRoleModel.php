<?php

namespace common\models;

use common\ActiveRecord\RbacPermissionRole;
use common\ActiveRecord\RbacPermission;
use Yii;
use yii\db\Exception;

/**
 * 角色权限关联表
 *
 * Class RbacPermissionRoleModel
 * @package common\models
 */
class RbacPermissionRoleModel
{

    /**
     * 根据角色获取分配的权限
     *
     * @param null $roleId
     *
     * @return array
     */
    public static function getAssignedPermissions( $roleId = null )
    {
        //获取所有权限
        $permissions = RbacPermission::find()
            ->with( 'childPermissions' )
            ->where( [ '=' , 'parent_id' , 0 ] )
            ->all();
        //获取角色拥有的权限
        $assignedPermissions = RbacPermissionRole::findAll( [ 'role_id' => $roleId ] );
        $iAssignedPermissions = [];
        foreach ( $assignedPermissions as $k => $v ) {
            $iAssignedPermissions[] = $v[ 'permission_id' ];
        }
        $iPermissions = [];
        //对比权限并标示管理员是否有权限
        foreach ( $permissions as $permission ) {
            $tmp = [];
            $tmp[ 'id' ] = $permission[ 'id' ];
            $tmp[ 'permission_name' ] = $permission[ 'permission_name' ];
            $tmp[ 'route_controller' ] = $permission[ 'route_controller' ];
            $tmp[ 'route_action' ] = $permission[ 'route_action' ];
            $tmp[ 'parent_id' ] = $permission[ 'parent_id' ];
            $tmp[ 'status' ] = $permission[ 'status' ];
            if ( in_array( $tmp[ 'id' ] , $iAssignedPermissions ) ) {
                $tmp[ 'has_permission' ] = 'true';
            } else {
                $tmp[ 'has_permission' ] = 'false';
            }
            foreach ( $permission[ 'childPermissions' ] as $childPermission ) {
                $_tmp = [];
                $_tmp[ 'id' ] = $childPermission[ 'id' ];
                $_tmp[ 'permission_name' ] = $childPermission[ 'permission_name' ];
                $_tmp[ 'route_controller' ] = $childPermission[ 'route_controller' ];
                $_tmp[ 'route_action' ] = $childPermission[ 'route_action' ];
                $_tmp[ 'parent_id' ] = $childPermission[ 'parent_id' ];
                $_tmp[ 'status' ] = $childPermission[ 'status' ];
                if ( in_array( $_tmp[ 'id' ] , $iAssignedPermissions ) ) {
                    $_tmp[ 'has_permission' ] = 'true';
                } else {
                    $_tmp[ 'has_permission' ] = 'false';
                }
                $tmp[ 'childPermission' ][] = $_tmp;
            }
            $iPermissions[] = $tmp;
        }
        return $iPermissions;
    }

    /**
     * 添加角色权限
     *
     * @param       $roleId
     * @param array $permissionIds
     *
     * @return bool
     */
    public static function assignRolePermissions( $roleId , $permissionIds = [] )
    {
        //删除角色权限
        if ( empty( $permissionIds ) ) {
            RbacPermissionRole::deleteAll( [ 'role_id' => $roleId ] );
            return true;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            //删除原来的权限
            RbacPermissionRole::deleteAll( [ 'role_id' => $roleId ] );
            //添加权限
            $data = [];
            foreach ( $permissionIds as $k => $j ) {
                $tmp = [ $roleId , $j ];
                $data[] = $tmp;
            }
            $key = [ 'role_id' , 'permission_id' ];
            Yii::$app->db->createCommand()->batchInsert( RbacPermissionRole::tableName() , $key , $data )->execute();
            $transaction->commit();
            return true;
        } catch ( Exception $e ) {
            $transaction->rollBack();
            return false;
        }
    }
}
