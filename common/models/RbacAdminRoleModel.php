<?php

namespace common\models;

use common\ActiveRecord\BackendAdmin;
use common\ActiveRecord\RbacAdminPermission;
use common\ActiveRecord\RbacAdminRole;
use Yii;

class RbacAdminRoleModel
{

    /**
     * 编辑管理员角色跟权限
     *
     * @param       $adminId
     * @param array $roleIds
     * @param array $permissionIds
     *
     * @return bool
     */
    public static function editAdminRole( $adminId , $roleIds = [] , $permissionIds = [] )
    {
        $admin = BackendAdmin::findOne( $adminId );
        if ( !$admin || empty( $admin ) ) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            //删除原来的角色id
            RbacAdminRole::deleteAll( [ 'admin_id' => $adminId ] );
            if ( !empty( $roleIds ) ) {
                //添加新的角色id
                $key = [ 'role_id' , 'admin_id' ];
                $values = [];
                foreach ( $roleIds as $k => $v ) {
                    $tmp = [ $v , $adminId ];
                    $values[] = $tmp;
                }
                Yii::$app->db->createCommand()->batchInsert( RbacAdminRole::tableName() , $key , $values )->execute();
            }
            //删除原来的权限
            RbacAdminPermission::deleteAll( [ 'admin_id' => $adminId ] );
            if ( !empty( $permissionIds ) ) {
                //添加新的权限id
                $key = [ 'admin_id' , 'permission_id' ];
                $values = [];
                foreach ( $permissionIds as $k => $v ) {
                    $tmp = [ $adminId , $v ];
                    $values[] = $tmp;
                }
                Yii::$app->db->createCommand()->batchInsert( RbacAdminPermission::tableName() , $key , $values )->execute();
            }
            $transaction->commit();
            return true;
        } catch ( \Exception $e ) {
            $transaction->rollBack();
            return false;
        }
    }
}
