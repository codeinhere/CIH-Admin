<?php

namespace common\models;

use common\ActiveRecord\RbacPermission;
use common\ActiveRecord\RbacAdminPermission;

class RbacAdminPermissionModel
{

    /**
     * 获取所有权限
     * 并检测管理员是否拥有权限并标示
     *
     * @param $adminId
     *
     * @return array
     */
    public static function getAdminPermissions( $adminId )
    {
        //获取所有权限
        $allPermissions = RbacPermission::find()
            ->with( [ 'childPermissions' ] )
            ->where( [ '=' , 'parent_id' , '0' ] )
            ->asArray()
            ->all();
        //获取已设置的权限
        $assignedPermissions = RbacAdminPermission::find()->where( [ 'admin_id' => $adminId ] )->asArray()->all();
        $iAssignedPermissions = [];
        foreach ( $assignedPermissions as $k => $v ) {
            $iAssignedPermissions[] = $v[ 'permission_id' ];
        }
        //检测管理员是否拥有权限并标示
        $setPermissions = [];
        foreach ( $allPermissions as $k => $v ) {
            $tmp = [];
            $tmp[ 'id' ] = $v[ 'id' ];
            $tmp[ 'permission_name' ] = $v[ 'permission_name' ];
            foreach ( $v[ 'childPermissions' ] as $i => $j ) {
                $_tmp[ 'id' ] = $j[ 'id' ];
                $_tmp[ 'permission_name' ] = $j[ 'permission_name' ];
                $_tmp[ 'status' ] = $j[ 'status' ];
                if ( in_array( $j[ 'id' ] , $iAssignedPermissions ) ) {
                    $_tmp[ 'has_permission' ] = 'true';
                } else {
                    $_tmp[ 'has_permission' ] = 'false';
                }
                $tmp[ 'childPermission' ][] = $_tmp;
            }
            $setPermissions[] = $tmp;
        }
        return $setPermissions;
    }
}
