<?php

namespace common\models;

use common\ActiveRecord\RbacRole;
use common\ActiveRecord\RbacAdminRole;
use Yii;

class RbacRoleModel
{

    /**
     * 账户状态：正常
     */
    const STATUS_ENABLE = 1;

    /**
     * 账户状态：禁用
     */
    const STATUS_DISABLE = 0;

    /**
     * 条件查询所有管理员
     *
     * @param $offset
     * @param $limit
     *
     * @return array
     */
    public static function getRoles( $offset , $limit )
    {
        $roleName = urldecode( Yii::$app->request->queryParams[ 'role_name' ] ?? null );
        $status = urldecode( Yii::$app->request->queryParams[ 'status' ] ?? null );
        $createBeginTime = urldecode( Yii::$app->request->queryParams[ 'cerate-begin-time' ] ?? null );
        if ( $createBeginTime != null ) {
            $createBeginTime = strtotime( $createBeginTime );
        }
        $createEndTime = urldecode( Yii::$app->request->queryParams[ 'create-end-time' ] ?? null );
        if ( $createEndTime != null ) {
            $createEndTime = strtotime( $createEndTime );
        }
        $query = RbacRole::find()
            //->where( [ '<>' , 'status' , RbacRoleModel::STATUS_DELETE ] )
            ->orderBy( [ 'create_time' => SORT_DESC ] )
            ->andFilterWhere( [ 'like' , 'role_name' , $roleName ] )
            ->andFilterWhere( [ '=' , 'status' , $status ] )
            ->andFilterWhere( [ '>=' , 'create_time' , $createBeginTime ] )
            ->andFilterWhere( [ '<=' , 'create_time' , $createEndTime ] );
        $data = $query->offset( $offset )
            ->limit( $limit )
            ->all();
        $count = $query->count();
        $pages = ceil( $count / $limit );
        $currentPage = $count / $limit - ( $count - $offset ) / $limit + 1;
        return [
            'data' => $data ,
            'pages' => $pages ,
            'count' => $count ,
            'currentPage' => $currentPage ,
        ];
    }

    /**
     * 获取所有角色
     * 并区分管理员是否拥有角色
     *
     * @param $adminId
     *
     * @return array
     */
    public static function getAssignAdminRoles( $adminId )
    {
        $roles = RbacRole::find()
            //->where( [ '<>' , 'status' , self::STATUS_DELETE ] )
            ->all();
        $assignedRoles = RbacAdminRole::findAll( [ 'admin_id' => $adminId ] );
        $roleIds = [];
        foreach ( $assignedRoles as $k => $v ) {
            $roleIds[] = $v[ 'role_id' ];
        }
        $data = [];
        foreach ( $roles as $k => $v ) {
            $tmp[ 'role_id' ] = $v[ 'id' ];
            $tmp[ 'role_name' ] = $v[ 'role_name' ];
            $tmp[ 'status' ] = $v[ 'status' ];
            if ( in_array( $v[ 'id' ] , $roleIds ) ) {
                $tmp[ 'has_role' ] = 'true';
            } else {
                $tmp[ 'has_role' ] = 'false';
            }
            $data[] = $tmp;
        }
        return $data;
    }

    /**
     * 根据名称查询角色
     *
     * @param $name
     * @param $limit
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchRoleByName( $name , $limit )
    {
        return RbacRole::find()
            ->andFilterWhere( [ 'like' , 'permission_name' , $name ] )
            ->limit( $limit )
            ->all();
    }
}
