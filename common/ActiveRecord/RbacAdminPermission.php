<?php

namespace common\ActiveRecord;

use Yii;

/**
 * This is the model class for table "rbac_admin_permission".
 *
 * @property int $admin_id      管理员id
 * @property int $permission_id 权限id
 */
class RbacAdminPermission extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_admin_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'admin_id' ] , 'required' ] ,
            [ [ 'admin_id' , 'permission_id' ] , 'integer' ] ,
            [ [ 'admin_id' ] , 'unique' ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'admin_id' => 'Admin ID' ,
            'permission_id' => 'Permission ID' ,
        ];
    }

    /**
     * 关联 permission 表
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany( RbacPermission::className() , [ 'id' => 'permission_id' ] );
    }
}
