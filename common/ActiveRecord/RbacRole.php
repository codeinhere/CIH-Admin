<?php

namespace common\ActiveRecord;

use Yii;

/**
 * This is the model class for table "rbac_role".
 *
 * @property int    $id          自增id
 * @property string $role_name   角色名称
 * @property int    $status      状态
 * @property int    $create_time 添加时间
 * @property int    $update_time 更新时间
 */
class RbacRole extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'role_name' ] , 'string' , 'max' => 255 ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID' ,
            'role_name' => 'Role Name' ,
        ];
    }
}
