<?php

namespace common\ActiveRecord;

use Yii;

/**
 * This is the model class for table "rbac_permission_role".
 *
 * @property int $permission_id 权限id
 * @property int $role_id       角色id
 */
class RbacPermissionRole extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_permission_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'permission_id' ] , 'required' ] ,
            [ [ 'permission_id' , 'role_id' ] , 'integer' ] ,
            [ [ 'permission_id' ] , 'unique' ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'permission_id' => 'Permission ID' ,
            'role_id' => 'Role ID' ,
        ];
    }

    /**
     * 关联 permission 表
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany( RbacPermission::className() , [ 'id' => 'permission_id' ] );
    }
}
