<?php

namespace common\ActiveRecord;

use common\models\RbacMenuModel;
use Yii;

/**
 * This is the model class for table "rbac_menu".
 *
 * @property int    $id             自增id
 * @property string $menu_name      菜单名称
 * @property int    $visible        是否可见0=不可见1=可见
 * @property int    $parent_id      父级id
 * @property int    $status         状态0=可用1=不可用
 * @property int    $admin_id       管理员id
 * @property int    $sort           排序
 * @property int    $permission_id  权限id
 * @property string $icon           图标
 */
class RbacMenu extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'visible' , 'parent_id' , 'status' , 'admin_id' , 'sort' ] , 'integer' ] ,
            [ [ 'menu_name' ] , 'string' , 'max' => 255 ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID' ,
            'menu_name' => 'Menu Name' ,
            'visible' => 'Visible' ,
            'parent_id' => 'Parent ID' ,
            'status' => 'Status' ,
            'admin_id' => 'Admin ID' ,
            'sort' => 'Sort' ,
        ];
    }

    /**
     * 关联权限表
     * @return \yii\db\ActiveQuery
     */
    public function getPermission()
    {
        return $this->hasOne( RbacPermission::className() , [ 'id' => 'permission_id' ] );
    }

    /**
     * 关联父级菜单
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne( RbacMenu::className() , [ 'id' => 'parent_id' ] );
    }

    /**
     * 获取子级菜单
     * @return \yii\db\ActiveQuery
     */
    public function getChildMenus()
    {
        return $this->hasMany( RbacMenu::className() , [ 'parent_id' => 'id' ] )->with( 'permission' )
            ->orderBy( [ 'sort' => SORT_ASC ] );
    }

    /**
     * 获取左侧菜单
     * @return $this
     */
    public function getChildMenusTree()
    {
        return $this->hasMany( RbacMenu::className() , [ 'parent_id' => 'id' ] )
            ->with( [ 'permission' ] )
            ->where( [ '=' , 'status' , RbacMenuModel::STATUS_ENABLE ] )
            ->andWhere( [ '=' , 'visible' , RbacMenuModel::VISIBLE_ENABLE ] )
            ->orderBy( [ 'sort' => SORT_ASC ] );
    }
}
