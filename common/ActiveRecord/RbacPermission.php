<?php

namespace common\ActiveRecord;

use common\models\RbacPermissionModel;
use Yii;

/**
 * This is the model class for table "rbac_permission".
 *
 * @property int    $id                    自增id
 * @property string $permission_name       权限名称
 * @property string $route_controller      控制器
 * @property string $route_action          方法
 * @property int    $parent_id             父级id
 * @property int    $status                状态0=禁用1=启用
 */
class RbacPermission extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID' ,
            'permission_name' => 'Permission Name' ,
            'route' => 'Route' ,
        ];
    }

    /**
     * 获取子级权限
     * @return \yii\db\ActiveQuery
     */
    public function getChildPermissions()
    {
        $controllerName = urldecode( Yii::$app->request->queryParams[ 'controller_name' ] ?? '' );
        $actionName = urldecode( Yii::$app->request->queryParams[ 'action_name' ] ?? '' );
        $permissionName = urldecode( Yii::$app->request->queryParams[ 'permission_name' ] ?? '' );
        return $this->hasMany( self::className() , [ 'parent_id' => 'id' ] )
            //->where( [ '=' , 'status' , RbacPermissionModel::STATUS_ENABLE ] )
            ->andFilterWhere( [ 'like' , 'permission_name' , $permissionName ] )
            ->andFilterWhere( [ 'like' , 'route_controller' , $controllerName ] )
            ->andFilterWhere( [ 'like' , 'route_action' , $actionName ] );
    }
}
