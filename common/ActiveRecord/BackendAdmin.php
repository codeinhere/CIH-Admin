<?php

namespace common\ActiveRecord;

use Yii;

/**
 * This is the model class for table "backend_admin".
 *
 * @property int    $id          自增id
 * @property string $username    用户名
 * @property string $password    密码
 * @property int    $register_time
 * @property int    $update_time 更新时间
 * @property int    $status      账户状态
 */
class BackendAdmin extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'backend_admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'register_time' , 'update_time' ] , 'integer' ] ,
            [ [ 'username' , 'password' ] , 'string' , 'max' => 255 ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID' ,
            'username' => 'Username' ,
            'password' => 'Password' ,
            'register_time' => 'Register Time' ,
            'update_time' => 'Update Time' ,
        ];
    }
}
