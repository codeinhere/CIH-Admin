<?php

namespace common\ActiveRecord;

use Yii;

/**
 * This is the model class for table "rbac_admin_role".
 *
 * @property int $role_id  角色id
 * @property int $admin_id 管理员id
 */
class RbacAdminRole extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_admin_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ [ 'role_id' ] , 'required' ] ,
            [ [ 'role_id' , 'admin_id' ] , 'integer' ] ,
            [ [ 'role_id' ] , 'unique' ] ,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID' ,
            'admin_id' => 'Admin ID' ,
        ];
    }

    /**
     * 关联角色表
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasMany( RbacRole::className() , [ 'id' => 'role_id' ] );
    }
}
