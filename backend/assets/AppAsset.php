<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        //'css/site.css' ,
        'css/adminlte/bower_components/bootstrap/dist/css/bootstrap.css' ,
        'css/adminlte/dist/css/AdminLTE.css' ,
        'css/adminlte/dist/css/skins/_all-skins.css' ,
        'css/adminlte/bower_components/font-awesome/css/font-awesome.css' ,
        //'css/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.scss' ,
        //'css/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css' ,
        //'css/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css' ,
        'css/adminlte/plugins/iCheck/all.css' ,
        'css/pagination.css' ,
        'css/common.css' ,
    ];

    public $js = [
        'css/adminlte/bower_components/jquery/dist/jquery.js' ,
        'css/adminlte/bower_components/jquery-ui/jquery-ui.js' ,
        'js/layer/layer.js' ,
        'js/pagination.js' ,
        'css/adminlte/bower_components/bootstrap/dist/js/bootstrap.js' ,
        //'css/adminlte/bower_components/bootstrap/js/affix.js' ,
        //'css/adminlte/bower_components/bootstrap/js/alert.js' ,
        //'css/adminlte/bower_components/bootstrap/js/button.js' ,
        //'css/adminlte/bower_components/bootstrap/js/carousel.js' ,
        //'css/adminlte/bower_components/bootstrap/js/collapse.js' ,
        //'css/adminlte/bower_components/bootstrap/js/dropdown.js' ,
        //'css/adminlte/bower_components/bootstrap/js/modal.js' ,
        //'css/adminlte/bower_components/bootstrap/js/popover.js' ,
        //'css/adminlte/bower_components/bootstrap/js/scrollspy.js' ,
        //'css/adminlte/bower_components/bootstrap/js/tab.js' ,
        //'css/adminlte/bower_components/bootstrap/js/tooltip.js' ,
        //'css/adminlte/bower_components/bootstrap/js/transition.js' ,
        //'css/adminlte/bower_components/moment/moment.js' ,
        //'css/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js' ,
        //'css/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js' ,
        'css/adminlte/plugins/iCheck/icheck.js' ,
        //'css/adminlte/bower_components/bootstrap/dist/js/npm.js' ,
        'css/adminlte/dist/js/adminlte.js' ,
        'js/common.js' ,
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
