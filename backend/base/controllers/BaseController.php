<?php

namespace backend\base\controllers;

/*classes*/
use common\ActiveRecord\RbacAdminPermission;
use common\models\RbacMenuModel;
use common\models\RbacPermissionModel;
use common\models\RbacRoleModel;
use yii\web\Controller;
use Yii;
/*ar*/
use common\ActiveRecord\RbacAdminRole;
use common\ActiveRecord\RbacMenu;
use common\ActiveRecord\RbacPermission;
use common\ActiveRecord\RbacPermissionRole;

/**
 * 底层控制器
 * Class BaseController
 * @package backend\base\controllers
 */
class BaseController extends Controller
{

    /**
     * 是否登录|默认未登录
     * @var bool
     */
    protected $isLogin = false;

    /**
     * 设置布局文件
     * @var string
     */
    public $layout = 'main';

    /**
     * 默认起始位置
     * @var int
     */
    protected $offset = 0;

    /**
     * 默认每页显示10条数据
     * @var int
     */
    protected $limit = 10;

    /**
     * 初始化函数
     */
    public function init()
    {
        parent::init();
        $this->checkLogin();
        $adminId = Yii::$app->session->get( 'admin_id' , '' );
        $this->authentication( $adminId );
        $this->setRequestRouteMenu();
    }

    /**
     * 检测管理员是否登录
     */
    private function checkLogin()
    {
        $session = Yii::$app->session;
        if ( $session->has( 'admin_id' ) ) {
            $this->isLogin = true;
            return true;
        }
        //跳转到登录页面
        header( 'Location:/web/login' );
        exit();
    }

    /**
     * 检测授权
     *
     * @param $adminId
     *
     * @return bool
     */
    private function authentication( $adminId )
    {
        //获取当前请求的 url
        $requestRoute = $this->getRequestRoute();
        if ( !$requestRoute ) {
            if ( Yii::$app->request->isAjax ) {
                echo $this->renderPartial( '//web/ajax_permission_deny' );
                exit();
            }
            $this->layout = 'main';
            $this->view->title = '403错误';
            echo $this->render( '//web/permission_deny' );
            exit();
        }
        //根据 request_route 获取权限
        $permission = $this->getPermissionByRoute( $requestRoute );
        if ( !$permission ) {
            if ( Yii::$app->request->isAjax ) {
                echo $this->renderPartial( '//web/ajax_permission_deny' );
                exit();
            }
            $this->layout = 'main';
            $this->view->title = '403错误';
            echo $this->render( '//web/permission_deny' );
            exit();
        }
        //根据 admin_id 获取 permission_id
        $adminPermissions = $this->getPermissionByAdmin( $adminId );
        //根据 admin_id 获取 role_id
        $roles = $this->getRolesByAdmin( $adminId );
        //根据 role_id 获取 permission_id
        $rolePermissions = $this->getPermissionsByRole( $roles );
        //验证
        if ( !in_array( $permission , $adminPermissions ) && !in_array( $permission , $rolePermissions ) ) {
            if ( Yii::$app->request->isAjax ) {
                echo $this->renderPartial( '//web/ajax_permission_deny' );
                exit();
            }
            $this->layout = 'main';
            $this->view->title = '403错误';
            echo $this->render( '//web/permission_deny' );
            exit();
        }
        return true;
    }

    /**
     * 获取角色拥有的可用权限
     *
     * @param $roles
     *
     * @return array
     */
    private function getPermissionsByRole( $roles )
    {
        $permission = RbacPermissionRole::find()
            ->with( [ 'permissions' ] )
            ->where( [ 'in' , 'role_id' , $roles ] )
            ->asArray()
            ->all();
        $permissionIds = [];
        foreach ( $permission as $k => $v ) {
            foreach ( $v[ 'permissions' ] as $i => $j ) {
                if ( $j[ 'status' ] == RbacPermissionModel::STATUS_ENABLE ) {
                    $permissionIds[] = $j[ 'id' ];
                }
            }
        }
        return $permissionIds;
    }

    /**
     * 获取管理员拥有的可用权限
     *
     * @param $adminId
     *
     * @return array
     */
    private function getPermissionByAdmin( $adminId )
    {
        $permissions = RbacAdminPermission::find()
            ->with( [ 'permissions' ] )
            ->where( [ '=' , 'admin_id' , $adminId ] )
            ->asArray()
            ->all();
        $permissionIds = [];
        foreach ( $permissions as $k => $v ) {
            foreach ( $v[ 'permissions' ] as $i => $j ) {
                if ( $j[ 'status' ] == RbacPermissionModel::STATUS_ENABLE ) {
                    $permissionIds[] = $j[ 'id' ];
                }
            }
        }
        return $permissionIds;
    }

    /**
     * 根据路由获取权限
     *
     * @param $route
     *
     * @return bool|int
     */
    private function getPermissionByRoute( $route )
    {
        $permission = RbacPermission::findOne( [ 'route_controller' => $route[ 'controllerId' ] , 'route_action' => $route[ 'actionId' ] , 'status' => RbacPermissionModel::STATUS_ENABLE ] );
        if ( !$permission || empty( $permission ) ) {
            return false;
        }
        return $permission->id;
    }

    /**
     * 获取角色
     *
     * @param $adminId
     *
     * @return array
     */
    private function getRolesByAdmin( $adminId )
    {
        $roles = RbacAdminRole::findAll( [ 'admin_id' => $adminId ] );
        $roles = RbacAdminRole::find()
            ->with( [ 'role' ] )
            ->asArray()
            ->all();
        $roleIds = [];
        foreach ( $roles as $k => $v ) {
            foreach ( $v[ 'role' ] as $i => $j ) {
                if ( $j[ 'status' ] == RbacRoleModel::STATUS_ENABLE ) {
                    $roleIds[] = $v[ 'role_id' ];
                }
            }
        }
        return $roleIds;
    }

    /**
     * 获取路由请求地址
     *
     * @return array|bool
     */
    private function getRequestRoute()
    {
        $request = $_REQUEST;
        $s = $request[ 's' ] ?? Yii::$app->defaultRoute;
        $s = substr( $s , 1 , strlen( $s ) - 1 );
        $ss = explode( '/' , $s );
        $controllerId = $ss[ 0 ] ?? '';
        $actionId = $ss[ 1 ] ?? '';
        if ( !preg_match( '/^[0-9a-zA-Z\-]*$/' , $controllerId ) ) {
            return false;//todo 设置错误信息
        }
        if ( !preg_match( '/^[0-9a-zA-Z\-]*$/' , $actionId ) ) {
            return false;//todo 设置错误信息
        }
        return [
            'controllerId' => $controllerId ,
            'actionId' => $actionId ,
        ];
    }

    /**
     * 设置返回json数据
     */
    protected function _json()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }

    /**
     * 检测控制器是否存在
     *
     * @param $controller
     *
     * @return bool
     */
    protected function checkBackendControllerExists( $controller )
    {
        $controllerDir = Yii::getAlias( '@backend' ) . '/controllers';
        $controllerFiles = scandir( $controllerDir );
        $controllers = [];
        foreach ( $controllerFiles as $file ) {
            if ( $file == '.' || $file == '..' ) {
                continue;
            }
            //截取控制器名称.eg:IndexController.php 截取后 Index
            $file = substr( $file , 0 , strlen( $file ) - 14 );
            //分割骆驼峰命名的控制器
            $controllerName = preg_replace( '/([A-Z])/' , ',\\1' , $file );
            //组合成数据
            $controllerNameArr = explode( ',' , $controllerName );
            //过滤空元素
            $controllerNameArr = array_filter( $controllerNameArr );
            $str = '';
            //循环骆驼峰命名的控制，转换成yii2格式的路由
            foreach ( $controllerNameArr as $k => $v ) {
                $v = strtolower( $v );
                $str .= $v . '-';
            }
            //去掉最后一个字符 -
            $str = substr( $str , 0 , strlen( $str ) - 1 );
            //把所有控制器组合成数组
            $controllers[] = $str;
        }
        //判断要添加的控制器是否存在
        return in_array( $controller , $controllers );
    }

    /**
     * 检测执行方法是否存在
     *
     * @param $controller
     * @param $action
     *
     * @return bool
     */
    protected function checkBackendActionExists( $controller , $action )
    {
        //转换控制器
        $controllerArr = explode( '-' , $controller );
        $str = '';
        //循环控制器拼接成骆驼峰命名方式
        foreach ( $controllerArr as $k => $v ) {
            $v = strtolower( $v );
            $v = ucfirst( $v );
            $str .= $v;
        }
        //组成文件名称
        $controllerFile = $str . 'Controller.php';
        //命名空间
        $controllerName = 'backend\controllers\\' . $str . 'Controller';
        //获取控制器文件
        $controllerFile = Yii::getAlias( '@backend' ) . '/controllers/' . $controllerFile;
        //检测文件
        if ( !file_exists( $controllerFile ) ) {
            return false;
        }
        //检测文件
        if ( !is_file( $controllerFile ) ) {
            return false;
        }
        //转换方法
        $actionArr = explode( '-' , $action );
        $actionStr = '';
        //循环方法拼接成骆驼峰命名方式
        foreach ( $actionArr as $k => $v ) {
            $v = strtolower( $v );
            $v = ucfirst( $v );
            $actionStr .= $v;
        }
        $actionName = 'action' . $actionStr;
        $reflectionClass = new \ReflectionClass( $controllerName );
        return $reflectionClass->hasMethod( $actionName );
    }

    /**
     * 设置菜单id
     */
    private function setRequestRouteMenu()
    {
        $route = $this->getRequestRoute();
        $adminId = Yii::$app->redis->hmget( 'admin_info' , 'admin_id' );
        $adminId = $adminId[ 0 ];
        $permission = RbacPermission::find()
            ->where( [ 'route_controller' => $route[ 'controllerId' ] ] )
            ->andWhere( [ 'route_action' => $route[ 'actionId' ] ] )
            ->asArray()
            ->one();
        //如果没有设置权限
        if ( empty( $permission ) || !$permission ) {
            Yii::$app->redis->set( $adminId . '_tree_menu_id' , 0 );
            return;
        }
        $menu = RbacMenu::find()
            ->where( [ 'permission_id' => $permission[ 'id' ] ] )
            ->asArray()
            ->all();
        //如果没有设置菜单
        if ( empty( $menu ) || !$menu ) {
            //查看上级url
            $urlReferrer = Yii::$app->request->referrer;
            $urlArr = parse_url( $urlReferrer );
            $s = $urlArr[ 'path' ];
            $s = substr( $s , 1 , strlen( $s ) - 1 );
            $ss = explode( '/' , $s );
            $controllerId = $ss[ 0 ] ?? '';
            $actionId = $ss[ 1 ] ?? '';
            //获取上级url的权限
            $permission = RbacPermission::find()
                ->where( [ 'route_controller' => $controllerId ] )
                ->andWhere( [ 'route_action' => $actionId ] )
                ->asArray()
                ->one();
            //获取上级url权限的菜单
            $menu = RbacMenu::find()
                ->where( [ 'permission_id' => $permission[ 'id' ] ] )
                ->asArray()
                ->all();
            $iTreeId = 0;
            //遍历设置父级菜单id
            foreach ( $menu as $k => $v ) {
                if ( $v[ 'parent_id' ] == RbacMenuModel::PARENT_ID ) {
                    $iTreeId = $v[ 'id' ];
                    break;
                }
            }
            //没有父级菜单id，遍历设置子级菜单id
            if ( $iTreeId == 0 ) {
                foreach ( $menu as $k => $v ) {
                    if ( $v[ 'parent_id' ] != RbacMenuModel::PARENT_ID ) {
                        $iTreeId = $v[ 'id' ];
                        break;
                    }
                }
            }
            Yii::$app->redis->set( $adminId . '_tree_menu_id' , $iTreeId );
            return;
        }
        $iTreeId = 0;
        //遍历设置父级菜单id
        foreach ( $menu as $k => $v ) {
            if ( $v[ 'parent_id' ] == RbacMenuModel::PARENT_ID ) {
                $iTreeId = $v[ 'id' ];
                break;
            }
        }
        //没有父级菜单id，遍历设置子级菜单id
        if ( $iTreeId == 0 ) {
            foreach ( $menu as $k => $v ) {
                if ( $v[ 'parent_id' ] != RbacMenuModel::PARENT_ID ) {
                    $iTreeId = $v[ 'id' ];
                    break;
                }
            }
        }
        Yii::$app->redis->set( $adminId . '_tree_menu_id' , $iTreeId );
    }
}
