<?php

namespace backend\controllers;

/*classes*/
use backend\base\controllers\BaseController;
use Yii;
/*active record*/
use common\ActiveRecord\RbacRole;
/*models*/
use common\models\RbacRoleModel;
use common\models\RbacPermissionModel;
use common\models\RbacPermissionRoleModel;

/**
 * 分配角色权限控制器
 * Class RbacRolePermissionController
 * @package backend\controllers
 */
class RbacRolePermissionController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认控制器
     * @var string
     */
    public $defaultAction = 'set-role-permission';

    /**
     * 分配角色权限
     * @return array|string
     */
    public function actionSetRolePermission()
    {
        $request = Yii::$app->request;
        if ( $request->isAjax ) {
            $this->_json();
            $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
            $limit = $request->queryParams[ 'size' ] ?? $this->limit;
            $query = RbacRoleModel::searchRoleByName( $name , $limit );
            $data = [];
            foreach ( $query as $k => $v ) {
                $tmp = [
                    'id' => $v[ 'id' ] ,
                    'text' => $v[ 'role_name' ] ,
                ];
                $data[] = $tmp;
            }
            return [ 'code' => 'success' , 'msg' => $data ];
        }
        $roles = RbacRole::find()->all();
        $iRoles = [];
        foreach ( $roles as $k => $v ) {
            $iRoles = [ $v[ 'id' ] , $v[ 'role_name' ] ];
        }
        $roleId = $request->queryParams[ 'roleId' ] ?? '';
        $permissions = RbacPermissionRoleModel::getAssignedPermissions( $roleId );
        $role = RbacRole::findOne( $roleId );
        return $this->render( 'set_role_permission' , [
            'roles' => $iRoles ,
            'permissions' => $permissions ,
            'role' => $role ,
        ] );
    }

    /**
     * 编辑分配角色权限
     * @return array
     */
    public function actionEditRolePermission()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求方式' ];
        }
        $permissionIds = $request->post( 'ids' );
        $roleId = $request->post( 'roleId' );
        return RbacPermissionRoleModel::assignRolePermissions( $roleId , $permissionIds )
            ? [ 'code' => 'success' , 'msg' => '保存成功' ]
            : [ 'code' => 'error' , 'msg' => '保存失败' ];
    }
}
