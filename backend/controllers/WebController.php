<?php

namespace backend\controllers;

use common\models\RbacRoleModel;
use yii\web\Controller;
use Yii;
use common\ActiveRecord\BackendAdmin;
use common\services\verifyCodeClass;

class WebController extends Controller
{

    /**
     * 不使用布局文件
     * @var bool
     */
    public $layout = false;

    /**
     * 初始化函数
     */
    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    /**
     * 登录
     * @return string
     */
    public function actionLogin()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ( $session->has( 'admin_id' ) ) {
            return $this->redirect( [ Yii::$app->defaultRoute ] );
        }
        if ( $request->isPost ) {
            $post = $request->post();
            $username = $post[ 'username' ];
            $password = $post[ 'password' ];
            $code = $post[ 'code' ];
            if ( !$code ) {
                return json_encode( [ 'code' => 'fail' , 'msg' => '请输入验证码' ] );
            }
            $_code = Yii::$app->redis->get( verifyCodeClass::REDIS_KEY_PREFIX . $code );
            if ( $_code != $code ) {
                return json_encode( [ 'code' => 'fail' , 'msg' => '验证码错误' ] );
            }
            $admin = BackendAdmin::findOne( [ 'username' => $username ] );
            if ( !$admin || empty( $admin ) ) {
                return json_encode( [ 'code' => 'fail' , 'msg' => '账号密码错误' ] );
            }
            if ( !password_verify( $password , $admin->password ) ) {
                return json_encode( [ 'code' => 'fail' , 'msg' => '账号密码错误' ] );
            }
            if ( $admin->status == RbacRoleModel::STATUS_DISABLE ) {
                return json_encode( [ 'code' => 'fail' , 'msg' => '该账号暂时不可用' ] );
            }
            $loginData = [
                'admin_id' => $admin->id ,
                'admin_username' => $admin->username ,
                'admin_register_time' => $admin->register_time ,
                'admin_update_time' => $admin->update_time
            ];
            $this->setLoginData( $loginData );
            return json_encode( [ 'code' => 'success' , 'msg' => '登录成功' ] );
        }
        $defaultRoute = Yii::$app->defaultRoute;
        $verifyCodeImage = $this->actionVerifyCodeImage();
        return $this->render( 'login' , [
            'defaultRoute' => $defaultRoute ,
            'verifyCodeImage' => $verifyCodeImage ,
        ] );
    }

    /**
     * 设置登录数据
     *
     * @param array $data
     */
    private function setLoginData( $data = [] )
    {
        $session = Yii::$app->session;
        $session->set( 'admin_id' , $data[ 'admin_id' ] );
        $session->set( 'admin_username' , $data[ 'admin_username' ] );
        Yii::$app->redis->hmset( 'admin_info' , 'admin_id' , $data[ 'admin_id' ] , 'admin_username' , $data[ 'admin_username' ] , 'admin_register_time' , $data[ 'admin_register_time' ] , 'admin_update_time' , $data[ 'admin_update_time' ] );
    }

    /**
     * 删除登录session数据
     */
    private function unsetLoginData()
    {
        //unset session
        $session = Yii::$app->session;
        $session->destroy();
        //delete redis
        $redis = Yii::$app->redis;
        $redis->del( 'admin_info' );
    }

    /**
     * 退出登录
     *
     */
    public function actionLogout()
    {
        $this->unsetLoginData();
        header( 'Location:/web/login' );
        exit();
    }

    /**
     * 获取验证码图片
     * @return string
     */
    public function actionVerifyCodeImage()
    {
        $imgObj = new verifyCodeClass();
        $imgAddress = $imgObj->getImage();
        $verifyCode = $imgObj->getCode();
        Yii::$app->redis->set( verifyCodeClass::REDIS_KEY_PREFIX . $verifyCode , $verifyCode );
        return Yii::$app->params[ 'webBaseUrl' ] . '/' . Yii::$app->params[ 'verifyCodeImageDir' ] . '/' . $imgAddress;
    }
}
