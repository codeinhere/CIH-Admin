<?php

namespace backend\controllers;

/*classes*/
use backend\base\controllers\BaseController;
use Yii;
use yii\db\Exception;
/*active record*/
use common\ActiveRecord\RbacRole;
use common\ActiveRecord\RbacPermissionRole;
use common\ActiveRecord\RbacAdminRole;
use common\ActiveRecord\RbacAdminPermission;
/*models*/
use common\models\RbacRoleModel;

/**
 * Class RbacRoleController
 * @package backend\controllers
 */
class RbacRoleController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认action
     * @var string
     */
    public $defaultAction = 'role';

    /**
     * 角色管理页面
     * @return string
     */
    public function actionRole()
    {
        $roles = RbacRoleModel::getRoles( $this->offset , $this->limit );
        return $this->render( 'role' , [
            'roles' => $roles[ 'data' ] ,
            'count' => $roles[ 'count' ] ,
            'pages' => $roles[ 'pages' ] ,
            'currentPage' => $roles[ 'currentPage' ] ,
            'defaultPageSize' => $this->limit ,
        ] );
    }

    /**
     * 添加角色
     * @return array
     */
    public function actionAddRole()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求方式' ];
        }
        $roleName = $request->post( 'roleName' );
        $status = $request->post( 'status' );
        if ( !$roleName ) {
            return [ 'code' => 'error' , 'msg' => '请填写名称' ];
        }
        if ( mb_strlen( $roleName ) > 10 ) {
            return [ 'code' => 'error' , 'msg' => '名称不能超过10个字 ' ];
        }
        if ( RbacRole::findOne( [ 'role_name' => $roleName ] ) ) {
            return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
        }
        $role = new RbacRole();
        $role->role_name = $roleName;
        $role->create_time = time();
        $role->status = $status;
        $role->update_time = time();
        return $role->save()
            ? [ 'code' => 'success' , 'msg' => '添加成功' ]
            : [ 'code' => 'error' , 'msg' => '添加失败' ];
    }

    /**
     * 异步请求角色数据
     * @return string
     */
    public function actionAjaxRoleQueryPage()
    {
        $roles = [];
        if ( Yii::$app->request->isGet ) {
            $roles = RbacRoleModel::getRoles( $this->offset , $this->limit );
        }
        return $this->renderAjax( '_roles' , [
            'roles' => $roles[ 'data' ] ,
            'offset' => $this->offset ,
            'limit' => $this->limit ,
        ] );
    }

    /**
     * 切换角色状态
     * @return array
     */
    public function actionToggleRoleStatus()
    {
        $this->_json();
        if ( !Yii::$app->request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求的方式' ];
        }
        $status = Yii::$app->request->post( 'status' );
        $id = Yii::$app->request->post( 'id' );
        //验证状态是否正确
        if ( $status != RbacRoleModel::STATUS_DISABLE && $status != RbacRoleModel::STATUS_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '未知错误，操作失败' ];
        }
        //验证用户是否存在
        $role = RbacRole::findOne( $id );
        if ( empty( $role ) ) {
            return [ 'code' => 'error' , 'msg' => '该角色不存在' ];
        }
        $role->status = $status;
        return $role->save()
            ? [ 'code' => 'success' , 'msg' => '操作成功' ]
            : [ 'code' => 'error' , 'msg' => '操作失败' ];
    }

    /**
     * 删除角色
     * @return array
     */
    public function actionDeleteRole()
    {
        $this->_json();
        if ( !Yii::$app->request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        $roleId = Yii::$app->request->post( 'id' );
        $transaction = Yii::$app->db->beginTransaction();
        try {
            //删除角色
            RbacRole::deleteAll( [ 'id' => $roleId ] );
            //获取该角色拥有的权限
            $permissions = RbacPermissionRole::findAll( [ 'role_id' => $roleId ] );
            $permissionIds = [];
            foreach ( $permissions as $k => $v ) {
                $permissionIds[] = $v[ 'permission_id' ];
            }
            //获取拥有该角色的管理员
            $admins = RbacAdminRole::findAll( [ 'role_id' => $roleId ] );
            $adminIds = [];
            foreach ( $admins as $k => $v ) {
                $adminIds[] = $v[ 'admin_id' ];
            }
            //获取管理员原有的权限
            //$oriPermissions = RbacAdminPermission::findAll( [ 'in' , 'admin_id' , $adminIds ] );
            $oriPermissions = RbacAdminPermission::find()
                ->where( [ 'in' , 'admin_id' , $adminIds ] )
                ->asArray()
                ->all();
            $iOriPermissions = [];
            foreach ( $oriPermissions as $k => $v ) {
                $tmp = [ $v[ 'admin_id' ] , $v[ 'permission_id' ] ];
                $iOriPermissions[] = $tmp;
            }
            //重组要插入的数据
            $data = [];
            foreach ( $adminIds as $k => $v ) {
                foreach ( $permissionIds as $i => $j ) {
                    $tmp = [ $v , $j ];
                    $data[] = $tmp;
                }
            }
            //将原有的权限跟要插入的数据进行比对。排除重复的
            foreach ( $iOriPermissions as $k => $v ) {
                foreach ( $data as $i => $j ) {
                    if ( $v[ 0 ] == $j[ 0 ] && $v[ 1 ] == $j[ 1 ] ) {
                        unset( $data[ $i ] );
                    }
                }
            }
            $key = [ 'admin_id' , 'permission_id' ];
            Yii::$app->db->createCommand()->batchInsert( RbacAdminPermission::tableName() , $key , $data )->execute();
            $transaction->commit();
            return [ 'code' => 'success' , 'msg' => '删除成功' ];
            //添加权限给管理员
        } catch ( Exception $e ) {
            $transaction->rollBack();
            return [ 'code' => 'error' , '删除失败' ];
        }
        //修改状态为删除
        RbacRole::updateAll( [ 'status' => RbacRoleModel::STATUS_DELETE ] , [ 'id' => $roleId ] );
        RbacRole::deleteAll( [ 'id' => $roleId ] );
        return [ 'code' => 'success' , 'msg' => '操作成功' ];
    }

    /**
     * 编辑角色数据
     * @return array
     */
    public function actionEditRole()
    {
        $request = Yii::$app->request;
        if ( $request->isPost ) {
            $this->_json();
            $id = $request->post( 'id' );
            $name = $request->post( 'name' );
            $status = $request->post( 'status' );
            $role = RbacRole::findOne( $id );
            if ( !$role ) {
                return [ 'code' => 'error' , 'msg' => '该数据不存在' ];
            }
            if ( !$name ) {
                return [ 'code' => 'error' , 'msg' => '请填写名称' ];
            }
            if ( mb_strlen( $name ) > 10 ) {
                return [ 'code' => 'error' , 'msg' => '名称长度不能超过10个字' ];
            }
            if ( !$status && $status != 0 ) {
                return [ 'code' => 'error' , 'msg' => '请选择状态值' ];
            }
            if ( $status != RbacRoleModel::STATUS_ENABLE && $status != RbacRoleModel::STATUS_DISABLE ) {
                return [ 'code' => 'error' , 'msg' => '该状态值不存在' ];
            }
            if ( $role->role_name != $name && RbacRole::findOne( [ 'role_name' => $name ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
            }
            $role->role_name = $name;
            $role->status = $status;
            $role->update_time = time();
            return $role->save()
                ? [ 'code' => 'success' , 'msg' => ' 保存成功' ]
                : [ 'code' => 'error' , 'msg' => '保存失败' ];
        }
        $id = Yii::$app->request->queryParams[ 'id' ] ?? '';
        $role = RbacRole::findOne( $id );
        return $this->renderAjax( 'edit_role' , [
            'role' => $role ,
        ] );
    }
}
