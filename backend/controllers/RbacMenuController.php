<?php

namespace backend\controllers;

/*classes*/
use backend\base\controllers\BaseController;
use Yii;
use yii\db\Exception;
/*active records*/
use common\ActiveRecord\BackendAdmin;
use common\ActiveRecord\RbacMenu;
use common\ActiveRecord\RbacPermission;
/*models*/
use common\models\BackendAdminModel;
use common\models\RbacMenuModel;
use common\models\RbacPermissionModel;

class RbacMenuController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认action
     * @var string
     */
    public $defaultAction = 'set-admin-menu';

    /**
     * 分配管理员菜单
     */
    public function actionSetAdminMenu()
    {
        $request = Yii::$app->request;
        $adminId = $request->queryParams[ 'adminId' ] ?? Yii::$app->session->get( 'admin_id' , '' );
        $menus = RbacMenuModel::getParentMenusByAdminId( $adminId );
        $admin = BackendAdmin::findOne( $adminId );
        if ( $request->isAjax ) {
            $this->_json();
            $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
            $limit = $request->queryParams[ 'size' ] ?? $this->limit;
            $query = BackendAdminModel::searchAdminByName( $name , $limit );
            $data = [];
            foreach ( $query as $k => $v ) {
                $tmp = [
                    'id' => $v[ 'id' ] ,
                    'text' => $v[ 'username' ] ,
                ];
                $data[] = $tmp;
            }
            return [ 'code' => 'success' , 'msg' => $data ];
        }
        return $this->render( 'set_admin_menu' , [
            'menus' => $menus ,
            'adminId' => $adminId ,
            //'adminUsername' => Yii::$app->session->get( 'admin_username' , '' ) ,
            'adminUsername' => $admin->username ?? '' ,
        ] );
    }

    /**
     * 异步查询父级菜单
     * @return array
     */
    public function actionAjaxSearchParentMenu()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isGet ) {
            return [];
        }
        $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
        $limit = $request->queryParams[ 'size' ] ?? $this->limit;
        $adminId = $request->queryParams[ 'adminId' ] ?? '';
        $query = RbacMenuModel::searchParentMenuByName( $name , $limit , $adminId );
        $data = [];
        foreach ( $query as $k => $v ) {
            $tmp = [
                'id' => $v[ 'id' ] ,
                'text' => $v[ 'menu_name' ] ,
            ];
            $data[] = $tmp;
        }
        return [ 'code' => 'success' , 'msg' => $data ];
    }

    /**
     * 删除菜单
     * @return array
     */
    public function actionDeleteMenu()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        $adminId = $request->post( 'adminId' );
        $menuId = $request->post( 'id' );
        $menu = RbacMenu::findOne( [ 'admin_id' => $adminId , 'id' => $menuId ] );
        if ( empty( $menu ) ) {
            return [ 'code' => 'success' , 'msg' => '删除成功' ];
        }
        //如果是父级菜单，则修改子级菜单的父级菜单 id 为 0
        if ( $menu[ 'parent_id' ] == RbacMenuModel::PARENT_ID ) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                //删除子级菜单 id
                //RbacMenu::updateAll( [ 'parent_id' => RbacMenuModel::PARENT_ID ] , [ 'parent_id' => $menuId ] );
                RbacMenu::deleteAll( [ 'parent_id' => $menuId , 'admin_id' => $adminId ] );
                //删除父级菜单
                RbacMenu::deleteAll( [ 'admin_id' => $adminId , 'id' => $menuId ] );
                $transaction->commit();
                return [ 'code' => 'success' , 'msg' => '删除成功' ];
            } catch ( Exception $e ) {
                $transaction->rollBack();
                return [ 'code' => 'error' , 'msg' => '删除失败' ];
            }
        }
        RbacMenu::deleteAll( [ 'admin_id' => $adminId , 'id' => $menuId ] );
        return [ 'code' => 'success' , 'msg' => '删除成功' ];
    }

    /**
     * 编辑菜单
     * @return string
     */
    public function actionEditMenu()
    {
        $request = Yii::$app->request;
        if ( $request->isPost ) {
            $this->_json();
            $id = $request->post( 'id' );
            $adminId = $request->post( 'adminId' );
            $name = trim( $request->post( 'name' ) );
            $isVisible = $request->post( 'isVisible' );
            $parentMenu = $request->post( 'parentMenu' );
            $permission = $request->post( 'permission' );
            $status = $request->post( 'status' );
            $sort = trim( $request->post( 'sort' ) );
            $menu = RbacMenu::findOne( [ 'id' => $id , 'admin_id' => $adminId ] );
            if ( !$menu ) {
                return [ 'code' => 'error' , 'msg' => '数据不存在.修改失败' ];
            }
            if ( !$name ) {
                return [ 'code' => 'error' , 'msg' => '请填写名称' ];
            }
            if ( mb_strlen( $name ) > 20 ) {
                return [ 'code' => 'error' , 'msg' => '名称不能超过20个字' ];
            }
            if ( $menu->menu_name != $name && RbacMenu::findOne( [ 'menu_name' => $name ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
            }
            if ( !$isVisible && $isVisible != 0 ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( !$status && $status != 0 ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $isVisible != RbacMenuModel::VISIBLE_DISABLE && $isVisible != RbacMenuModel::VISIBLE_ENABLE ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $status != RbacMenuModel::STATUS_ENABLE && $status != RbacMenuModel::STATUS_DISABLE ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $sort ) {
                if ( !is_numeric( $sort ) ) {
                    return [ 'code' => 'error' , 'msg' => '排序值只能是数字' ];
                }
                if ( $sort > 99999999 ) {
                    return [ 'code' => 'error' , 'msg' => '排序值超过最大值，请重新填写' ];
                }
            }
            if ( $parentMenu ) {
                if ( !RbacMenu::findOne( [ 'id' => $parentMenu , 'parent_id' => RbacMenuModel::PARENT_ID , 'status' => RbacMenuModel::STATUS_ENABLE ] ) ) {
                    return [ 'code' => 'error' , 'msg' => '该父级菜单不存在' ];
                }
                $menu->parent_id = $parentMenu;
            }
            if ( $permission ) {
                if ( !RbacPermission::findOne( [ 'id' => $permission , 'status' => RbacPermissionModel::STATUS_ENABLE ] ) ) {
                    return [ 'code' => 'error' , 'msg' => '该权限不存在' ];
                }
                $menu->permission_id = $permission;
            }
            $menu->menu_name = $name;
            $menu->visible = $isVisible;
            $menu->status = $status;
            $menu->sort = $sort;
            return $menu->save()
                ? [ 'code' => 'success' , 'msg' => '修改成功' ]
                : [ 'code' => 'error' , 'msg' => '修改失败' ];
        }
        $id = $request->queryParams[ 'id' ] ?? '';
        $adminId = $request->queryParams[ 'adminId' ] ?? '';
        $menu = RbacMenuModel::getMenuByIdAndAdminId( $id , $adminId );
        return $this->renderAjax( 'edit_admin_menu' , [
            'menu' => $menu ,
            'adminId' => $adminId ,
        ] );
    }

    /**
     * 添加菜单
     * @return array
     */
    public function actionAddAdminMenu()
    {
        $this->_json();
        $request = Yii::$app->request;
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        $adminId = $request->post( 'adminId' );
        $menuName = $request->post( 'menuName' );
        $isParent = $request->post( 'isParent' );
        $parentMenuId = $request->post( 'parentMenu' );
        $permissionId = $request->post( 'permission' );
        $status = $request->post( 'status' );
        $isVisible = $request->post( 'isVisible' );
        $sort = $request->post( 'sort' );
        $icon = $request->post( 'icon' );
        $admin = BackendAdmin::findOne( $adminId );
        if ( !$admin || empty( $admin ) ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        if ( !$menuName ) {
            return [ 'code' => 'error' , 'msg' => '请填写名称' ];
        }
        if ( mb_strlen( $menuName ) > 20 ) {
            return [ 'code' => 'error' , 'msg' => '名称不能超过20个字' ];
        }
        if ( !$isParent && $isParent != 0 ) {
            return [ 'code' => 'error' , 'msg' => '请选择是否是父级菜单' ];
        }
        if ( $isParent != RbacMenuModel::IS_PARENT && $isParent != RbacMenuModel::IS_NOT_PARENT ) {
            return [ 'code' => 'error' , 'msg' => '数据错误.添加失败' ];
        }
        if ( $parentMenuId ) {
            if ( empty( RbacMenu::findOne( [ 'id' => $parentMenuId , 'parent_id' => RbacMenuModel::PARENT_ID , 'admin_id' => $adminId ] ) ) ) {
                return [ 'code' => 'error' , 'msg' => '该父级菜单不存在' ];
            }
        } else {
            $parentMenuId = 0;
        }
        if ( $permissionId ) {
            $permission = RbacPermission::findOne( $permissionId );
            if ( empty( $permission ) ) {
                return [ 'code' => 'error' , 'msg' => '该权限不存在' ];
            }
        } else {
            $permissionId = 0;
        }
        if ( !$status && $status != RbacMenuModel::STATUS_DISABLE ) {
            return [ 'code' => 'error' , 'msg' => '请选择状态' ];
        }
        if ( $status != RbacMenuModel::STATUS_DISABLE && $status != RbacMenuModel::STATUS_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '数据错误.添加失败' ];
        }
        if ( !$isVisible && $isVisible != RbacMenuModel::VISIBLE_DISABLE ) {
            return [ 'code' => 'error' , 'msg' => '请选择是否可见' ];
        }
        if ( $isVisible != RbacMenuModel::VISIBLE_DISABLE && $isVisible != RbacMenuModel::VISIBLE_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '数据错误.添加失败' ];
        }
        if ( $sort ) {
            if ( !is_numeric( $sort ) ) {
                return [ 'code' => 'error' , 'msg' => '排序只能是数字' ];
            }
            if ( $sort > 99999999 ) {
                return [ 'code' => 'error' , 'msg' => '排序数值超过系统限制' ];
            }
        } else {
            $sort = 0;
        }
        if ( $icon ) {
            if ( mb_strlen( $icon ) > 20 ) {
                return [ 'code' => 'error' , 'msg' => '图标字符不能超过20个字' ];
            }
        } else {
            $icon = '';
        }
        $menu = new RbacMenu();
        $menu->menu_name = $menuName;
        $menu->visible = $isVisible;
        $menu->parent_id = $parentMenuId;
        $menu->status = $status;
        $menu->admin_id = $adminId;
        $menu->sort = $sort;
        $menu->permission_id = $permissionId;
        $menu->icon = $icon;
        return $menu->save()
            ? [ 'code' => 'success' , 'msg' => '添加成功' ]
            : [ 'code' => 'error' , 'msg' => '添加失败' ];
    }

    /**
     * 编辑父级菜单
     * @return array|string
     */
    public function actionEditParentMenu()
    {
        $request = Yii::$app->request;
        if ( $request->isPost ) {
            $this->_json();
            $id = $request->post( 'id' );
            $adminId = $request->post( 'adminId' );
            $name = trim( $request->post( 'name' ) );
            $isVisible = $request->post( 'isVisible' );
            //$parentMenu = $request->post( 'parentMenu' );
            $permission = $request->post( 'permission' );
            $status = $request->post( 'status' );
            $sort = trim( $request->post( 'sort' ) );
            $menu = RbacMenu::findOne( [ 'id' => $id , 'admin_id' => $adminId ] );
            if ( !$menu ) {
                return [ 'code' => 'error' , 'msg' => '数据不存在.修改失败' ];
            }
            if ( !$name ) {
                return [ 'code' => 'error' , 'msg' => '请填写名称' ];
            }
            if ( mb_strlen( $name ) > 20 ) {
                return [ 'code' => 'error' , 'msg' => '名称不能超过20个字' ];
            }
            if ( $menu->menu_name != $name && RbacMenu::findOne( [ 'menu_name' => $name ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
            }
            if ( !$isVisible && $isVisible != 0 ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( !$status && $status != 0 ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $isVisible != RbacMenuModel::VISIBLE_DISABLE && $isVisible != RbacMenuModel::VISIBLE_ENABLE ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $status != RbacMenuModel::STATUS_ENABLE && $status != RbacMenuModel::STATUS_DISABLE ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $sort ) {
                if ( !is_numeric( $sort ) ) {
                    return [ 'code' => 'error' , 'msg' => '排序值只能是数字' ];
                }
                if ( $sort > 99999999 ) {
                    return [ 'code' => 'error' , 'msg' => '排序值超过最大值，请重新填写' ];
                }
            }
            if ( $permission ) {
                if ( !RbacPermission::findOne( [ 'id' => $permission , 'status' => RbacPermissionModel::STATUS_ENABLE ] ) ) {
                    return [ 'code' => 'error' , 'msg' => '该权限不存在' ];
                }
                $menu->permission_id = $permission;
            }
            $menu->menu_name = $name;
            $menu->visible = $isVisible;
            $menu->status = $status;
            $menu->sort = $sort;
            return $menu->save()
                ? [ 'code' => 'success' , 'msg' => '修改成功' ]
                : [ 'code' => 'error' , 'msg' => '修改失败' ];
        }
        $isParent = $request->queryParams[ 'isParent' ] ?? '';
        $id = $request->queryParams[ 'id' ] ?? '';
        $adminId = $request->queryParams[ 'adminId' ] ?? '';
        $menu = RbacMenuModel::getMenuByIdAndAdminId( $id , $adminId );
        return $this->renderAjax( 'edit_admin_parent_menu' , [
            'menu' => $menu ,
            'adminId' => $adminId ,
            'isParent' => $isParent ,
        ] );
    }

    /**
     * 异步查询权限
     * @return array
     */
    public function actionAjaxSearchPermission()
    {
        $request = Yii::$app->request;
        if ( !$request->isGet ) {
            return [];
        }
        $this->_json();
        $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
        $limit = $request->queryParams[ 'size' ] ?? $this->limit;
        $query = RbacPermissionModel::searchPermissionByName( $name , $limit );
        $data = [];
        foreach ( $query as $k => $v ) {
            $tmp = [
                'id' => $v[ 'id' ] ,
                'text' => $v[ 'permission_name' ] ,
            ];
            $data[] = $tmp;
        }
        return [ 'code' => 'success' , 'msg' => $data ];
    }
}
