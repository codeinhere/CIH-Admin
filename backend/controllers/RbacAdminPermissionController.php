<?php

namespace backend\controllers;

use backend\base\controllers\BaseController;
use common\ActiveRecord\BackendAdmin;
use Yii;

/*active records*/
/*models*/
use common\models\BackendAdminModel;
use common\models\RbacRoleModel;
use common\models\RbacAdminPermissionModel;
use common\models\RbacAdminRoleModel;

/**
 * 分配管理员权限控制器
 * Class RbacAdminPermissionController
 * @package backend\controllers
 */
class RbacAdminPermissionController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认action
     * @var string
     */
    public $defaultAction = 'set-admin-permission';

    /**
     * 分配管理员权限
     * @return array|string
     */
    public function actionSetAdminPermission()
    {
        $request = Yii::$app->request;
        if ( $request->isAjax ) {
            $this->_json();
            $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
            $limit = $request->queryParams[ 'size' ] ?? $this->limit;
            $query = BackendAdminModel::searchAdminByName( $name , $limit );
            $data = [];
            foreach ( $query as $k => $v ) {
                $tmp = [
                    'id' => $v[ 'id' ] ,
                    'text' => $v[ 'username' ] ,
                ];
                $data[] = $tmp;
            }
            return [ 'code' => 'success' , 'msg' => $data ];
        }
        $adminId = $request->queryParams[ 'adminId' ] ?? null;
        $admin = BackendAdmin::findOne( $adminId );
        $roles = RbacRoleModel::getAssignAdminRoles( $adminId );
        $permissions = RbacAdminPermissionModel::getAdminPermissions( $adminId );
        return $this->render( 'set_admin_permission' , [
            'admin' => $admin ,
            'roles' => $roles ,
            'permissions' => $permissions ,
        ] );
    }

    /**
     * 编辑管理员权限
     * @return array
     */
    public function actionEditAdminPermission()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        $adminId = $request->post( 'adminId' );
        $roleIds = $request->post( 'roleIds' );
        $permissionIds = $request->post( 'permissionIds' );
        $result = RbacAdminRoleModel::editAdminRole( $adminId , $roleIds , $permissionIds );
        return $result
            ? [ 'code' => 'success' , 'msg' => '保存成功' ]
            : [ 'code' => 'error' , 'msg' => '保存失败' ];
    }
}
