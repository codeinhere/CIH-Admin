<?php

namespace backend\controllers;

use backend\base\controllers\BaseController;
use Yii;
/*active records*/
use common\ActiveRecord\BackendAdmin;
/*models*/
use common\models\BackendAdminModel;

/**
 * 管理员控制器
 * Class BackendAdminController
 * @package backend\controllers
 */
class BackendAdminController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认action
     * @var string
     */
    public $defaultAction = 'admin';

    /**
     * 管理员管理页面
     * @return string
     */
    public function actionAdmin()
    {
        $admins = BackendAdminModel::getAdmins( $this->offset , $this->limit );
        return $this->render( 'admin' , [
            'admins' => $admins[ 'data' ] ,
            'count' => $admins[ 'count' ] ,
            'pages' => $admins[ 'pages' ] ,
            'currentPage' => $admins[ 'currentPage' ] ,
            'defaultPageSize' => $this->limit ,
        ] );
    }

    /**
     * 添加管理员
     * @return array
     */
    public function actionAddAdmin()
    {
        $request = Yii::$app->request;
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求方式' ];
        }
        $username = trim( $request->post( 'username' ) );
        $password = trim( $request->post( 'password' ) );
        $status = trim( $request->post( 'status' ) );
        //返回json数据
        $this->_json();
        //验证是否填写用户名
        if ( !$username ) {
            return [ 'code' => 'error' , 'msg' => '请填写账号' ];
        }
        //验证用户名规则
        if ( !preg_match( '/^[_0-9a-zA-Z]*$/' , $username ) ) {
            return [ 'code' => 'error' , 'msg' => '账号只能是数字大小写字母下划线组成' ];
        }
        //验证用户名是否存在
        $sql = 'select `id` from ' . BackendAdmin::tableName() . ' where `username` = :username';
        $query = Yii::$app->db->createCommand( $sql )->bindParam( ':username' , $username )->queryOne();
        if ( $query ) {
            return [ 'code' => 'error' , 'msg' => '该账号已存在' ];
        }
        if ( strlen( $username ) < 6 || strlen( $username ) > 16 ) {
            return [ 'code' => 'error' , 'msg' => '账号不能小于6位或者16位字符' ];
        }
        if ( !$password ) {
            return [ 'code' => 'error' , 'msg' => '请填写密码' ];
        }
        if ( !preg_match( '/^[0-9a-zA-Z]*$/' , $password ) ) {
            return [ 'code' => 'error' , 'msg' => '密码只能是数字大小写字母组成' ];
        }
        //验证账号状态
        if ( $status != BackendAdminModel::STATUS_DISABLE && $status != BackendAdminModel::STATUS_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '发生错误.失败' ];
        }
        $passwordHash = password_hash( $password , PASSWORD_DEFAULT );
        $execute = Yii::$app->db->createCommand()->insert( BackendAdmin::tableName() , [
            'username' => $username ,
            'password' => $passwordHash ,
            'register_time' => time() ,
            'update_time' => time() ,
            'status' => $status ,
        ] )->execute();
        if ( !$execute ) {
            return [ 'code' => 'error' , 'msg' => '添加失败' ];
        }
        return [ 'code' => 'success' , 'msg' => '' ];
    }

    /**
     * 异步请求管理员数据
     * @return string
     */
    public function actionAjaxQueryAdminPage()
    {
        $admins = [];
        if ( Yii::$app->request->isGet ) {
            $admins = BackendAdminModel::getAdmins( $this->offset , $this->limit );
            //$admins = PermissionModel::getAdmins( $this->offset , $this->limit );
        }
        return $this->renderAjax( '_admins' , [
            'admins' => $admins[ 'data' ] ,
            'offset' => $this->offset ,
            'limit' => $this->limit ,
        ] );
    }

    /**
     * 切换管理员状态
     * @return array
     */
    public function actionToggleAdminStatus()
    {
        $this->_json();
        if ( !Yii::$app->request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求的方式' ];
        }
        $status = Yii::$app->request->post( 'status' );
        $id = Yii::$app->request->post( 'id' );
        //验证状态是否正确
        if ( $status != BackendAdminModel::STATUS_DISABLE && $status != BackendAdminModel::STATUS_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '未知错误，操作失败' ];
        }
        //验证用户是否存在
        $admin = BackendAdmin::findOne( $id );
        if ( empty( $admin ) ) {
            return [ 'code' => 'error' , 'msg' => '该管理员账号不存在' ];
        }
        $admin->status = $status;
        $admin->update_time = time();
        return $admin->save()
            ? [ 'code' => 'success' , 'msg' => '' ]
            : [ 'code' => 'error' , 'msg' => '操作失败' ];
    }

    /**
     * 删除管理员
     * @return array
     */
    public function actionDeleteAdmin()
    {
        $this->_json();
        if ( !Yii::$app->request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '没有权限' ];
        }
        $adminId = Yii::$app->request->post( 'id' );
        return BackendAdminModel::deleteAdmin( $adminId )
            ? [ 'code' => 'success' , 'msg' => '删除成功' ]
            : [ 'code' => 'error' , 'msg' => '删除失败' ];
    }

    /**
     * 编辑管理员
     * @return array|string
     */
    public function actionEditAdmin()
    {
        $request = Yii::$app->request;
        if ( $request->isPost ) {
            $this->_json();
            $id = $request->post( 'id' );
            $username = $request->post( 'username' );
            $password = $request->post( 'password' );
            $status = $request->post( 'status' );
            $admin = BackendAdmin::findOne( $id );
            if ( empty( $admin ) ) {
                return [ 'code' => 'error' , 'msg' => '该管理员不存在' ];
            }
            if ( empty( $username ) ) {
                return [ 'code' => 'error' , 'msg' => '请填写账号' ];
            }
            if ( $admin[ 'username' ] != $username && BackendAdmin::findOne( [ 'username' => $username ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该账号已存在' ];
            }
            if ( mb_strlen( $username ) < 6 ) {
                return [ 'code' => 'error' , 'msg' => '账号不能小于6个字符' ];
            }
            if ( mb_strlen( $username ) > 16 ) {
                return [ 'code' => 'error' , 'msg' => '账号不能大于16个字符' ];
            }
            if ( !preg_match( '/^[_0-9a-zA-Z]*$/' , $username ) ) {
                return [ 'code' => 'error' , 'msg' => '账号只能是大小写字母数字下划线组成' ];
            }
            if ( $password ) {
                if ( mb_strlen( $password ) < 6 ) {
                    return [ 'code' => 'error' , 'msg' => '密码长度不能小于6个字符' ];
                }
                if ( mb_strlen( $password ) > 16 ) {
                    return [ 'code' => 'error' , 'msg' => '密码长度不能大于16个字符' ];
                }
                if ( !preg_match( '/^[0-9a-zA-Z]*$/' , $password ) ) {
                    return [ 'code' => 'error' , 'msg' => '密码只能数大小写字母数字组成' ];
                }
                $admin->password = password_hash( $password , PASSWORD_DEFAULT );
            }
            if ( $status != 0 && empty( $status ) ) {
                return [ 'code' => 'error' , 'msg' => '请选择状态' ];
            }
            if ( $status != BackendAdminModel::STATUS_ENABLE && $status != BackendAdminModel::STATUS_DISABLE ) {
                return [ 'code' > 'error' , 'msg' => '状态值错误' ];
            }
            $admin->username = $username;
            $admin->status = $status;
            $admin->update_time = time();
            return $admin->save()
                ? [ 'code' => 'success' , 'msg' => '保存成功' ]
                : [ 'code' => 'error' , 'msg' => '保存失败' ];
        }
        $id = $request->queryParams[ 'id' ] ?? '';
        $admin = BackendAdmin::findOne( $id );
        return $this->renderAjax( 'edit_admin' , [
            'admin' => $admin ,
        ] );
    }

    /**
     * 展示管理员详情页
     * @return string
     */
    public function actionDetailAdmin()
    {
        $request = Yii::$app->request;
        $id = $request->queryParams[ 'id' ] ?? '';
        $admin = BackendAdmin::findOne( $id );
        return $this->renderAjax( 'detail_admin' , [
            'admin' => $admin ,
        ] );
    }
}
