<?php

namespace backend\controllers;

/*classes*/
use backend\base\controllers\BaseController;
use Yii;

/*active records*/
/*models*/
use common\ActiveRecord\RbacPermission;
use common\models\RbacPermissionModel;

class RbacPermissionController extends BaseController
{

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * 默认action
     * @var string
     */
    public $defaultAction = 'permission';

    /**
     * 权限管理页面
     * @return string
     */
    public function actionPermission()
    {
        //添加权限
        //$permissions = PermissionModel::getPermissions();
        $permissions = RbacPermissionModel::getPermissions();
        return $this->render( 'permission' , [
            'permissions' => $permissions ,
        ] );
    }

    /**
     * 添加权限
     * @return array
     */
    public function actionAddPermission()
    {
        $request = Yii::$app->request;
        $name = $request->post( 'name' );
        $controller = $request->post( 'controller' );
        $action = $request->post( 'action' );
        $parentId = $request->post( 'parent' );
        $status = $request->post( 'status' );
        if ( !$parentId ) {
            $parentId = 0;
        }
        $this->_json();
        if ( !$name ) {
            return [ 'code' => 'error' , 'msg' => '请填写名称' ];
        }
        if ( mb_strlen( $name ) > 20 ) {
            return [ 'code' => 'error' , 'msg' => '名称不能超过20个字' ];
        }
        /*
        if ( RbacPermission::findOne( [ 'permission_name' => $name ] ) ) {
            return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
        }
        */
        //子级权限
        if ( $parentId != 0 ) {
            if ( RbacPermission::findOne( [ 'permission_name' => $name , 'parent_id' => $parentId ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
            }
            if ( !$controller ) {
                return [ 'code' => 'error' , 'msg' => '请填写控制器' ];
            }
            if ( !$action ) {
                return [ 'code' => 'error' , 'msg' => '请填写方法' ];
            }
            if ( RbacPermission::findOne( [ 'route_controller' => $controller , 'route_action' => $action , 'parent_id' => $parentId ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该权限已存在' ];
            }
            //验证控制器是否存在
            if ( !$this->checkBackendControllerExists( $controller ) ) {
                return [ 'code' => 'error' , 'msg' => '该控制器不存在' ];
            }
            //验证方法是否存在
            if ( !$this->checkBackendActionExists( $controller , $action ) ) {
                return [ 'code' => 'error' , 'msg' => '该方法不存在' ];
            }
        } else {//父级权限
            if ( RbacPermission::findOne( [ 'permission_name' => $name ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该名称已存在' ];
            }
            if ( $controller ) {
                if ( !$action ) {
                    return [ 'code' => 'error' , 'msg' => '请填写方法' ];
                }
                if ( RbacPermission::findOne( [ 'route_controller' => $controller , 'route_action' => $action ] ) ) {
                    return [ 'code' => 'error' , 'msg' => '该权限已存在' ];
                }
            }
            if ( $action ) {
                if ( !$controller ) {
                    return [ 'code' => 'error' , 'msg' => '请填写控制器' ];
                }
                if ( RbacPermission::findOne( [ 'route_controller' => $controller , 'route_action' => $action ] ) ) {
                    return [ 'code' => 'error' , 'msg' => '该权限已存在' ];
                }
            }
        }
        if ( !$status && $status != RbacPermissionModel::STATUS_DISABLE ) {
            return [ 'code' => 'error' , 'msg' => '该状态值不存在' ];
        }
        if ( $status != RbacPermissionModel::STATUS_DISABLE && $status != RbacPermissionModel::STATUS_ENABLE ) {
            return [ 'code' => 'error' , 'msg' => '该状态值不存在' ];
        }
        $permission = ( new RbacPermission() );
        $permission->permission_name = $name;
        $permission->route_controller = $controller;
        $permission->route_action = $action;
        $permission->parent_id = $parentId;
        $permission->status = $status;
        return $permission->save()
            ? [ 'code' => 'success' , 'msg' => '添加成功' ]
            : [ 'code' => 'error' , 'msg' => '添加失败' ];
    }

    /**
     * 删除权限
     * @return array
     */
    public function actionDeletePermission()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求方式' ];
        }
        $ids = $request->post( 'ids' );
        if ( !is_array( $ids ) ) {
            return [ 'code' => 'error' , 'msg' => '错误的数据格式' ];
        }
        RbacPermission::deleteAll( [ 'id' => $ids ] );
        return [ 'code' => 'success' , 'msg' => '删除成功' ];
    }

    /**
     * 删除父级权限
     * @return array
     */
    public function actionDeleteParentPermission()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isPost ) {
            return [ 'code' => 'error' , 'msg' => '错误的请求方式' ];
        }
        $id = $request->post( 'id' );
        $transaction = Yii::$app->db->beginTransaction();
        try {
            RbacPermission::deleteAll( [ 'id' => $id ] );
            RbacPermission::deleteAll( [ 'parent_id' => $id ] );
            $transaction->commit();
            return [ 'code' => 'success' , 'msg' => '删除成功' ];
        } catch ( \Exception $e ) {
            $transaction->rollBack();
            return [ 'code' => 'error' , 'msg' => '删除失败' ];
        }
    }

    /**
     * 异步查询权限
     * @return array
     */
    public function actionAjaxSearchPermission()
    {
        $request = Yii::$app->request;
        if ( !$request->isGet ) {
            return [];
        }
        $this->_json();
        $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
        $limit = $request->queryParams[ 'size' ] ?? $this->limit;
        $query = RbacPermissionModel::searchPermissionByName( $name , $limit );
        $data = [];
        foreach ( $query as $k => $v ) {
            $tmp = [
                'id' => $v[ 'id' ] ,
                'text' => $v[ 'permission_name' ] ,
            ];
            $data[] = $tmp;
        }
        return [ 'code' => 'success' , 'msg' => $data ];
    }

    /**
     * 异步查询父级权限
     * @return array
     */
    public function actionAjaxSearchParentPermission()
    {
        $request = Yii::$app->request;
        $this->_json();
        if ( !$request->isGet ) {
            return [];
        }
        $name = $request->queryParams[ 'search' ][ 'term' ] ?? '';
        $limit = $request->queryParams[ 'size' ] ?? $this->limit;
        $query = RbacPermissionModel::searchParentPermissionByName( $name , $limit );
        $data = [];
        foreach ( $query as $k => $v ) {
            $tmp = [
                'id' => $v[ 'id' ] ,
                'text' => $v[ 'permission_name' ] ,
            ];
            $data[] = $tmp;
        }
        return [ 'code' => 'success' , 'msg' => $data ];
    }

    /**
     * 编辑权限
     * @return string
     */
    public function actionEditPermission()
    {
        $request = Yii::$app->request;
        if ( $request->isPost ) {
            $this->_json();
            $permissionName = $request->post( 'permissionName' );
            $controllerName = $request->post( 'controllerName' );
            $actionName = $request->post( 'actionName' );
            $editId = $request->post( 'editId' );
            $status = $request->post( 'status' );
            $permission = RbacPermission::findOne( $editId );
            if ( !$permission ) {
                return [ 'code' => 'error' , 'msg' => '该数据不存在' ];
            }
            if ( !$permissionName ) {
                return [ 'code' => 'error' , 'msg' => '请填写权限名称' ];
            }
            if ( !$controllerName ) {
                return [ 'code' => 'error' , 'msg' => '请填写控制器' ];
            }
            if ( !$actionName ) {
                return [ 'code' => 'error' , 'msg' => '请填写方法' ];
            }
            if ( $permissionName != $permission->permission_name && RbacPermission::findOne( [ 'permission_name' => $permissionName ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该权限名称已存在' ];
            }
            if ( !$this->checkBackendControllerExists( $controllerName ) ) {
                return [ 'code' => 'error' , 'msg' => '该控制器不存在' ];
            }
            //验证方法是否存在
            if ( !$this->checkBackendActionExists( $controllerName , $actionName ) ) {
                return [ 'code' => 'error' , 'msg' => '该方法不存在' ];
            }
            if ( ( $controllerName != $permission->route_controller && $actionName != $permission->route_action ) && RbacPermission::findOne( [ 'route_controller' => $controllerName , 'route_action' => $actionName ] ) ) {
                return [ 'code' => 'error' , 'msg' => '该权限已存在 ' ];
            }
            if ( !$status && $status != RbacPermissionModel::STATUS_DISABLE ) {
                return [ 'code' => 'error' , 'msg' => '数据错误.修改失败' ];
            }
            if ( $status != RbacPermissionModel::STATUS_DISABLE && $status != RbacPermissionModel::STATUS_ENABLE ) {
                return [ 'code' => 'error' , 'msg' => '状态值不存在' ];
            }
            $permission->permission_name = $permissionName;
            $permission->route_controller = $controllerName;
            $permission->route_action = $actionName;
            $permission->status = $status;
            return $permission->save() ?
                [ 'code' => 'success' , 'msg' => '' ] :
                [ 'code' => 'error' , 'msg' => '修改失败' ];
        }
        $id = $request->queryParams[ 'id' ] ?? '';
        $permission = RbacPermission::findOne( $id );
        return $this->renderAjax( 'edit_permission' , [
            'permission' => $permission ,
        ] );
    }
}
