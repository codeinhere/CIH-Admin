/**
 * 公共js文件
 * 依赖jquery
 */
var jq = $;
/**
 * 获取url请求参数
 * @param param
 * @returns {null}
 */
function getQueryParams(param) {
    var reg = new RegExp("(^|&)" + param + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}

/**
 * 获取相对路径
 * @returns {string|*}
 */
function getUrlRelativePath() {
    var url = document.location.toString();
    var arrUrl = url.split("//");

    var start = arrUrl[1].indexOf("/");
    var relUrl = arrUrl[1].substring(start);//stop省略，截取从start开始到结尾的所有字符

    if (relUrl.indexOf("?") != -1) {
        relUrl = relUrl.split("?")[0];
    }
    return relUrl;
}

/**
 * 判断是否为null
 * @param data
 * @returns {boolean}
 */
function isNull(data) {
    if (data == "" || data == undefined || data == null) {
        return true;
    }
    return false;
}

/**
 * 去掉头尾空字符串
 * @param str
 */
function trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

/**
 * 检测是否是金额
 * @param money
 * @returns {boolean}
 */
function pregMatchMoney(money) {
    var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,4})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
    return reg.test(money);
    //000
    //0
    //0.
    //0.0
    //050
    //00050.12
    //70.1
    //70.11
    //70.11111
    //500
}

/**
 * 检测是否是数字
 * @param number
 * @returns {boolean}
 */
function isNumber(number) {
    var reg = /^[0-9]*$/;
    return reg.test(number);
}

/**
 * 检测是否是浮点数
 * @param number
 * @returns {boolean}
 */
function isPositiveFloat(number) {
    var reg = /^\d+(\.\d+)?$/;
    return reg.test(number);
}

/**
 * 检测是否是整数
 * @param number
 * @returns {boolean}
 */
function isPositiveInteger(number) {
    var reg = /^[0-9]*[1-9][0-9]*$/;
    return reg.test(number);
}
/**
 * 检测是否为email
 * @param email
 * @returns {boolean}
 */
function checkEmail(email) {
    return /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/.test(email);
}

/**
 * 检测是否为手机号码
 * @param phone
 * @returns {boolean}
 */
function checkPhone(phone) {
    if (!(/^1[34578]\d{9}$/.test(phone))) {
        return false;
    }
    return true;
}

/**
 * 数组转化成json字符串
 * @param o
 * @returns {*}
 */
function arrayToJson(o) {
    var r = [];
    if (typeof o == "string") return "\"" + o.replace(/([\'\"\\])/g, "\\$1").replace(/(\n)/g, "\\n").replace(/(\r)/g, "\\r").replace(/(\t)/g, "\\t") + "\"";
    if (typeof o == "object") {
        if (!o.sort) {
            for (var i in o)
                r.push(i + ":" + arrayToJson(o[i]));
            if (!!document.all && !/^\n?function\s*toString\(\)\s*\{\n?\s*\[native code\]\n?\s*\}\n?\s*$/.test(o.toString)) {
                r.push("toString:" + o.toString.toString());
            }
            r = "{" + r.join() + "}";
        } else {
            for (var i = 0; i < o.length; i++) {
                r.push(arrayToJson(o[i]));
            }
            r = "[" + r.join() + "]";
        }
        return r;
    }
    return o.toString();
}
/**
 * 替换参数值
 * @param paramName
 * @param replaceWith
 */
function replaceParamVal(paramName, replaceWith) {
    var oUrl = this.location.href.toString();
    var re = eval('/(' + paramName + '=)([^&]*)/gi');
    var nUrl = oUrl.replace(re, paramName + '=' + replaceWith);
    return nUrl;
    //this.location = nUrl;
}

/**
 * 带参数分页跳转
 */
function iReloadPage(offset, _time) {
    var searchStr = window.location.search;
    //var offset = jq("#offset").val();
    var url = "";
    if (searchStr.match(/offset/)) {
        //window.location.href = replaceParamVal("offset", offset);
        url = replaceParamVal("offset", offset);
    } else {
        if (searchStr.length == 0) {
            url = "?offset=" + offset;
        } else {
            url = searchStr + "&offset=" + offset;
        }
        //window.location.href = url;
    }
    setTimeout(function () {
        window.location.href = url;
    }, _time);
}
