<?php
if ( !isset( $_SESSION ) ) {
    session_start();
}
defined( 'YII_DEBUG' ) or define( 'YII_DEBUG' , true );
defined( 'YII_ENV' ) or define( 'YII_ENV' , 'dev' );
/*
$request = $_REQUEST;
$s = $request[ 's' ];
$s = substr( $s , 1 , strlen( $s ) - 1 );
$ss = explode( '/' , $s );
print_r( $ss );
die;
*/
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';
$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php' ,
    require __DIR__ . '/../../common/config/main-local.php' ,
    require __DIR__ . '/../config/main.php' ,
    require __DIR__ . '/../config/main-local.php'
);
( new yii\web\Application( $config ) )->run();
