<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->title = '分配角色权限';
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
    <section class="content-header">
        <h1>分配权限</h1>
        <ol class="breadcrumb">
            <li>
                <a href="/"><i class="fa fa-dashboard"></i>首页</a>
            </li>
            <li class="active">
                <?= $this->title ?>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="admins-data-table" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div id="page-length" class="dataTables_length">
                                    <span>分配角色:
                                        <select aria-hidden="true" tabindex="-1" id="select-role"
                                                class="select2-hidden-accessible"
                                                style="width: 100%;" name="role_name">
                                            <?php if ( empty( $role ) ) : ?>
                                                <option value="" selected="selected">选择角色</option>
                                            <?php else : ?>
                                                <option value="<?= $role->id ?>"
                                                        selected="selected"><?= $role->role_name ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div id="ajax-query-container">
                                <?php if ( empty( $permissions ) ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                没有数据
                                            </p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="button" class="btn btn-primary btn-sm" id="save-permission">保存
                                        </button>
                                    </div>
                                </div>
                                <?php foreach ( $permissions as $permission ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box box-solid">
                                                <div class="box-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            父级权限:<b><?= $permission[ 'permission_name' ] ?></b>
                                                            <?php if ( empty( $permission[ 'childPermission' ] ) ) : ?>
                                                                <p class="text-muted well well-sm no-shadow"
                                                                   style="margin-top: 10px;">
                                                                    没有数据
                                                                </p>
                                                            <?php else : ?>
                                                                <ul>
                                                                    <?php foreach ( $permission[ 'childPermission' ] as $childPermission ) : ?>
                                                                        <li style="list-style: none;">
                                                                            <?php if ( $childPermission[ 'has_permission' ] == 'false' ) : ?>
                                                                                <?php if ( $childPermission[ 'status' ] == 1 ) : ?>
                                                                                    <input type="checkbox"
                                                                                           value="<?= $childPermission[ 'id' ] ?>"
                                                                                           class="permission_check">

                                                                                <?php else : ?>
                                                                                    <input disabled type="checkbox"
                                                                                           value="<?= $childPermission[ 'id' ] ?>"
                                                                                           class="permission_check">
                                                                                    <label
                                                                                            class="label label-warning no-padding"><span
                                                                                                class="fa fa-ban"></span>
                                                                                        已禁用</label>

                                                                                <?php endif; ?>
                                                                            <?php else : ?>
                                                                                <?php if ( $childPermission[ 'status' ] == 1 ) : ?>
                                                                                    <input type="checkbox"
                                                                                           checked="checked"
                                                                                           value="<?= $childPermission[ 'id' ] ?>"
                                                                                           class="permission_check">

                                                                                <?php else : ?>
                                                                                    <input disabled type="checkbox"
                                                                                           checked="checked"
                                                                                           value="<?= $childPermission[ 'id' ] ?>"
                                                                                           class="permission_check">
                                                                                    <label
                                                                                            class="label label-warning no-padding"><span
                                                                                                class="fa fa-ban"></span>已禁用</label>

                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                            <?= $childPermission[ 'permission_name' ] ?>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            <?php endif; ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <?php
                                $js = <<<JS
                                
    //icheck 样式
    jq(".permission_check").iCheck({
        checkboxClass : "icheckbox_minimal-blue",
    });
    //异步请求角色权限 start
    function ajaxQueryRolePermission() {
          //var roleId = jq(this).val();
          var roleId = jq("#select-role").val();
          //var index = layer.load(1);
          jq.ajax({
              url : "/rbac-role-permission/ajax-query-role-permission" ,
              data : { roleId : roleId } ,
              dataType : "html" ,
              type : "GET" ,
              success : function( data ){
                  //layer.close(index);
                  jq("#ajax-query-container").html(data);
              },//success end
          });
    }
    //异步请求角色权限 end
    //icheck 样式
    //保存 start
    jq("#save-permission").on("click",function(){
        var ids = [];
        //var roleId = jq("#role-id").val();
        var roleId = jq("#select-role").val();
        if(isNull(roleId)){
            layer.msg("请选择角色",{
                icon : 0 ,
                time : 1000 ,
            });
            return;
        }
        jq(".permission_check").each(function(){
            if(jq(this).prop("checked")){
                var id = jq(this).val();
                ids.push(id);
            }
        });
        var index = layer.load(1);
        jq.ajax({
            url : "/rbac-role-permission/edit-role-permission" ,
            data : { ids : ids , roleId : roleId } ,
            dataType : "json" ,
            type : "POST" ,
            success : function(data){
                layer.close(index);
                if( data.code == "success" ){
                    layer.msg("保存成功",{
                        icon : 1 ,
                        time : 1000 ,
                    });
                    setTimeout("ajaxQueryRolePermission()" , 1100);
                }
                if( data.code == "error" ){
                    layer.msg(data.msg,{
                        icon : 2 ,
                        time : 1000 ,
                    })
                }
            },//success end
        });
    });
    //保存 end
JS;
                                //$this->registerJs( $js );
                                ?>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$js = <<<JS
    jq.fn.modal.Constructor.prototype.enforceFocus = function () { };
    //icheck 样式
    jq(".permission_check").iCheck({
        checkboxClass : "icheckbox_minimal-blue",
    });
    //icheck 样式
    //保存 start
    jq("#save-permission").on("click",function(){
        var ids = [];
        //var roleId = jq("#role-id").val();
        var roleId = jq("#select-role").val();
        if(isNull(roleId)){
            layer.msg("请选择角色 ",{
                icon : 0 ,
                time : 1000 ,
            });
            return ;
        }
        jq(".permission_check").each(function(){
            if(jq(this).prop("checked")){
                var id = jq(this).val();
                ids.push(id);
            }
        });
        var index = layer.load(1);
        jq.ajax({
            url : "/rbac-role-permission/edit-role-permission" ,
            data : { ids : ids , roleId : roleId } ,
            dataType : "json" ,
            type : "POST" ,
            success : function(data){
                layer.close(index);
                if( data.code == "success" ){
                    layer.msg("保存成功",{
                        icon : 1 ,
                        time : 1000 ,
                    });
                    setTimeout("window.location.reload()" , 1100);
                }
            },//success end
        });
    });
    //保存 end
    
    //select role start
    jq("#select-role").select2({
        ajax : {
            url : "/rbac-role-permission/set-role-permission" ,
            dataType : "json" ,
            type : "GET" ,
            data : function( term , size ){
                return {
                    search : term ,
                    size : 10 ,
                };
            },//data end
            processResults : function( data ){
                return {
                    results : data.msg ,
                };
            },//results end
        },
    });
    //select role end
    
    //select change start
    jq("#select-role").on("change",function(){
        var roleId = jq(this).val();
        var index = layer.load(1);
        window.location.href = '/rbac-role-permission/set-role-permission?roleId=' + roleId;
        /*
        jq.ajax({
            url : "/rbac-role-permission/ajax-query-role-permission" ,
            data : { roleId : roleId } ,
            dataType : "html" ,
            type : "GET" ,
            success : function( data ){
                layer.close(index);
                jq("#ajax-query-container").html(data);
            },//success end
        });
        */
    });
    //select change end
    
JS;
$this->registerJs( $js );
?>