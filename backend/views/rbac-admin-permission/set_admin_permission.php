<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->title = '设置管理员权限';
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
<section class="content-header">
    <h1>分配角色</h1>
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-dashboard"></i>首页</a>
        </li>
        <li class="active">
            <?= $this->title ?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div id="admins-data-table" class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-xs-3">
                                <div id="page-length" class="dataTables_length">
                                    <span>分配管理员:
                                        <select aria-hidden="true" tabindex="-1" id="select-admin"
                                                class="select2-hidden-accessible"
                                                style="width: 100%;" name="username">
                                            <?php if ( empty( $admin ) ) : ?>
                                                <option value="" selected="selected">选择管理员</option>
                                            <?php else : ?>
                                                <option value="<?= $admin->id ?>"
                                                        selected="selected"><?= $admin->username ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id="ajax-query-container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-primary btn-sm" id="save-permission">保存
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <h3>分配角色</h3>
                                    <?php if ( empty( $roles ) ) : ?>
                                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            没有数据
                                        </p>
                                    <?php else : ?>
                                        <ul class="list-unstyled">
                                            <?php foreach ( $roles as $role ) : ?>
                                                <li style="margin-bottom: 3px;">
                                                    <?php if ( $role[ 'has_role' ] == 'false' ) : ?>
                                                        <?php if ( $role[ 'status' ] == 1 ) : ?>
                                                            <input name="hasRole" type="checkbox"
                                                                   value="<?= $role[ 'role_id' ] ?>">
                                                        <?php else : ?>
                                                            <input name="hasRole" type="checkbox" disabled
                                                                   value="<?= $role[ 'role_id' ] ?>"><label
                                                                    class="label label-warning no-padding"><span
                                                                        class="fa fa-ban"></span>已禁用</label>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <?php if ( $role[ 'status' ] == 1 ) : ?>
                                                            <input name="hasRole" type="checkbox"
                                                                   value="<?= $role[ 'role_id' ] ?>"
                                                                   checked>
                                                        <?php else : ?>
                                                            <input name="hasRole" type="checkbox" disabled
                                                                   value="<?= $role[ 'role_id' ] ?>"
                                                                   checked><label
                                                                    class="label label-warning no-padding"><span
                                                                        class="fa fa-ban"></span>已禁用</label>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?= $role[ 'role_name' ] ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <?php
                                        $js = <<<JS
                                        jq(document).ready(function(){
                                            //icheck start
                                            jq("input[name=\"hasRole\"]").iCheck({
                                                checkboxClass : "icheckbox_minimal-blue",
                                            });
                                            //icheck end
                                        });
JS;
                                        $this->registerJs( $js );
                                        ?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-xs-6">
                                    <h3>分配权限</h3>
                                    <?php if ( empty( $permissions ) ) : ?>
                                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            没有数据
                                        </p>
                                    <?php else : ?>
                                        <?php foreach ( $permissions as $permission ) : ?>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <?= $permission[ 'permission_name' ] ?>
                                                    <?php if ( empty( $permission[ 'childPermission' ] ) ) : ?>
                                                    <?php else : ?>
                                                        <ul>
                                                            <?php foreach ( $permission[ 'childPermission' ] as $childPermission ) : ?>
                                                                <li style="list-style: none;">
                                                                    <!--判断是否有权限 start-->
                                                                    <?php if ( $childPermission[ 'has_permission' ] == 'false' ) : ?>
                                                                        <!--判断状态 start-->
                                                                        <?php if ( $childPermission[ 'status' ] == 1 ) : ?>
                                                                            <input name="permission" type="checkbox"
                                                                                   value="<?= $childPermission[ 'id' ] ?>"
                                                                                   class="permission_check">

                                                                            <!--判断状态 start-->
                                                                        <?php else : ?>
                                                                            <input disabled name="permission"
                                                                                   type="checkbox"
                                                                                   value="<?= $childPermission[ 'id' ] ?>"
                                                                                   class="permission_check"><label
                                                                                    class="label label-warning no-padding"><span
                                                                                        class="fa fa-ban"></span>已禁用
                                                                            </label>
                                                                        <?php endif; ?>
                                                                    <?php else : ?>
                                                                        <?php if ( $childPermission[ 'status' ] == 1 ) : ?>
                                                                            <input name="permission" type="checkbox"
                                                                                   checked="checked"
                                                                                   value="<?= $childPermission[ 'id' ] ?>"
                                                                                   class="permission_check">

                                                                        <?php else : ?>
                                                                            <input disabled name="permission"
                                                                                   type="checkbox"
                                                                                   checked="checked"
                                                                                   value="<?= $childPermission[ 'id' ] ?>"
                                                                                   class="permission_check"><label
                                                                                    class="label label-warning no-padding"><span
                                                                                        class="fa fa-ban"></span>已禁用</label>

                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                    <?= $childPermission[ 'permission_name' ] ?>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </li>
                                            </ul>
                                        <?php endforeach; ?>
                                        <?php
                                        $js = <<<JS
                                        jq(document).ready(function(){
                                            //icheck start
                                            jq("input[name=\"permission\"]").iCheck({
                                                checkboxClass : "icheckbox_minimal-blue",
                                            });
                                            //icheck end
                                        });
JS;
                                        $this->registerJs( $js );
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $js = <<<JS
                        jq(document).ready(function(){
                            jq.fn.modal.Constructor.prototype.enforceFocus = function () { };
                            //select admin start
                            jq("#select-admin").select2({
                                ajax : {
                                    url : "/rbac-admin-permission/set-admin-permission" ,
                                    dataType : "json" ,
                                    type : "GET" ,
                                    data : function( term , size ){
                                        return {
                                            search : term ,
                                            size : 10 ,
                                        };
                                    },//data end
                                    processResults : function( data ){
                                        return {
                                            results : data.msg ,
                                        };
                                    },//results end
                                },
                            });
                            //select admin end
                            //保存 start
                            jq("#save-permission").on("click",function(){
                                var adminId = jq("#select-admin").val();
                                var roleIds = [];
                                var permissionIds = [];
                                if(isNull(adminId)){
                                    layer.msg("请选择管理员",{
                                        icon : 0 ,
                                        time : 1000 ,
                                    });
                                    return;
                                }
                                //获取角色id
                                jq("input[name=\"hasRole\"]").each(function(){
                                    if(jq(this).prop("checked")){
                                        var id = jq(this).val();
                                        roleIds.push(id);
                                    }
                                });
                                //获取权限id
                                jq("input[name=\"permission\"]").each(function(){
                                    if(jq(this).prop("checked")){
                                        var id = jq(this).val();
                                        permissionIds.push(id);
                                    }
                                });
                                var index = layer.load(1);
                                jq.ajax({
                                    url : "/rbac-admin-permission/edit-admin-permission" ,
                                    data : { "adminId" : adminId , "roleIds" : roleIds , "permissionIds" : permissionIds} ,
                                    dataType : "json" ,
                                    type : "POST" ,
                                    success : function( data ){
                                        layer.close(index);
                                        if( data.code == "error" ){
                                            layer.msg(data.msg , {
                                                icon : 2 ,
                                                time : 1000 ,
                                            });
                                            return ;
                                        }
                                        if( data.code == "success" ){
                                            layer.msg( "保存成功" , {
                                                icon : 1 ,
                                                time : 1000 ,
                                                
                                            } );
                                            setTimeout("window.location.reload()" , 1100);
                                        }
                                    }
                                });
                            });
                            //保存 end
                            //change start
                            jq("#select-admin").on("change",function(){
                                var adminId = jq(this).val();
                                var index = layer.load(1);
                                window.location.href = '/rbac-admin-permission/set-admin-permission?adminId=' + adminId;
                                /*
                                jq.ajax({
                                    url : "/rbac-admin-permission/ajax-query-admin-permission" ,
                                    data : { adminId : adminId } ,
                                    dataType : "html" ,
                                    type : "GET" ,
                                    success : function( data ){
                                        layer.close(index);
                                        jq("#ajax-query-container").html(data);
                                    },//success end
                                });
                                */
                            });
                            //change end
                        });

JS;
                        $this->registerJs( $js );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>