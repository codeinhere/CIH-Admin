<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
?>
<?php if ( empty( $admin ) ) : ?>
    <p>没有数据</p>
<?php else : ?>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1" style="margin-top:10px;">
            <table class="table table-bordered">
                <tbody>
                <tr role="row">
                    <td style="width: 20%;" class="text-right"><b>管理员账号</b></td>
                    <td style="width: 80%;"><?= $admin[ 'username' ] ?></td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="text-right"><b>注册时间</b></td>
                    <td style="width: 80%;"><?= date( 'Y-m-d H:i:s' , $admin[ 'register_time' ] ) ?></td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="text-right"><b>更新数据时间</b></td>
                    <td style="width: 80%;"><?= date( 'Y-m-d H:i:s' , $admin[ 'update_time' ] ) ?></td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="text-right"><b>账号状态</b></td>
                    <td>
                        <?php if ( $admin[ 'status' ] == 1 ) : ?>
                            <span class="label label-success">正常</span>
                        <?php else : ?>
                            <span class="label label-warning">禁用</span>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>