<?php
/**
 * 管理员管理页面
 */
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->title = '管理员管理';
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.zh-CN.min.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
    <section class="content-header">
        <h1>所有管理员
            <small>管理所有管理员</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/"><i class="fa fa-dashboard"></i>首页</a>
            </li>
            <li class="active">
                <?= $this->title ?>
            </li>
        </ol>
    </section>
    <input type="hidden" id="default-page-size" value="<?= $defaultPageSize ?>">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="admins-data-table" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div id="page-length" class="dataTables_length">
                                        <label>每页显示
                                            <select id="limit" class="form-control input-sm search-param"
                                                    name="page-length"
                                                    aria-controls="example1">
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>条数据
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-6 pull-right">
                                    <button class="btn btn-primary btn-sm pull-right" id="open-add-page">添加管理员</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box-body">
                                        <div class="form-group no-margin">
                                            <label class="control-label">账号</label>
                                            <input type="text" id="admin_username"
                                                   class="form-control input-sm search-param"
                                                   style="width:100px;">
                                        </div>
                                        <div class="form-group no-margin">
                                            <label class="control-label">状态</label>
                                            <select class="form-control search-param" id="status">
                                                <option value="">选择状态</option>
                                                <option value="0">禁用</option>
                                                <option value="1">启用</option>
                                            </select>
                                        </div>
                                        <div class="form-group no-margin">
                                            <label class="control-label">添加时间</label>
                                            <input type="text" id="create-begin-time" value=""
                                                   class="form-control input-sm search-param"
                                                   style="width: 100px;">
                                            -
                                            <input type="text" id="create-end-time"
                                                   class="input-sm form-control search-param"
                                                   style="width: 100px;">
                                        </div>
                                        <div class="form-group no-margin">
                                            <button class="btn btn-primary btn-sm" id="search">搜索</button>
                                            <button class="btn btn-info btn-sm" id="reset">重置</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ajax-query-container">
                                <?php if ( empty( $admins ) ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                没有数据
                                            </p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-bordered table-hover dataTable" id="admins-list"
                                                   role="grid" aria-describedby="admins-info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc">账号</th>
                                                    <th class="sorting_asc">状态</th>
                                                    <th class="sorting_asc">添加时间</th>
                                                    <th class="sorting_asc">更新时间</th>
                                                    <th class="sorting_asc">操作</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ( $admins as $admin ) : ?>
                                                    <tr role="row">
                                                        <td>
                                                            <?= $admin[ 'username' ] ?>
                                                        </td>
                                                        <td>
                                                            <?php if ( $admin[ 'status' ] == 0 ) : ?>
                                                                <button type="button"
                                                                        class="btn btn-xs btn-flat btn-default">禁用
                                                                </button>
                                                            <?php else : ?>
                                                                <button type="button"
                                                                        class="btn btn-xs btn-flat btn-success">启用
                                                                </button>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?= date( 'Y-m-d H:i:s' , $admin[ 'register_time' ] ) ?>
                                                        </td>
                                                        <td>
                                                            <?= date( 'Y-m-d H:i:s' , $admin[ 'update_time' ] ) ?>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-info btn-xs action-btn actions no-margin no-padding"
                                                                    data-action="detail"
                                                                    data-id="<?= $admin[ 'id' ] ?>"><span
                                                                        class="fa fa-eye"></span>详情
                                                            </button>
                                                            <button class="btn btn-primary btn-xs action-btn actions no-margin no-padding"
                                                                    data-action="edit" data-id="<?= $admin[ 'id' ] ?>">
                                                                <span class="fa fa-edit"></span>编辑
                                                            </button>
                                                            <button class="btn btn-danger btn-xs action-btn actions no-margin no-padding"
                                                                    data-action="delete"
                                                                    data-id="<?= $admin[ 'id' ] ?>"><span
                                                                        class="fa fa-trash"></span>删除
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-5" style="margin-top:-10px;">
                                    <div class="dataTables_info">
                                        当前在第&nbsp;<b id="current-page"><?= $currentPage ?></b>/<b
                                                id="pages"><?= $pages ?></b>&nbsp;页.总共有&nbsp;<b><i
                                                    id="count"><?= $count ?></i></b>&nbsp;条数据
                                    </div>
                                </div>
                                <div class="col-xs-7" style="margin-top:-10px;" id="pagination">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="add-admin" tabindex="-1" role="dialog" aria-labelledby="addAdminModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addAdminModalLabel">添加管理员</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>账号</label>
                        <input type="text" class="form-control" id="username">
                    </div>
                    <div class="form-group">
                        <label>密码</label>
                        <input type="password" class="form-control" id="password">
                    </div>
                    <div class="form-group">
                        <label>重复密码</label>
                        <input type="password" class="form-control" id="repeat-password">
                    </div>
                    <div class="form-group">
                        <label>账号状态</label>
                        <br/>
                        正常<input type="radio" name="status" value="1" checked>
                        禁用<input type="radio" name="status" value="0">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="add">添加</button>
                </div>
                <?php
                $js = <<<JS
                jq(document).ready(function(){
                    //添加 start
                    jq("#add").click(function(){
                        var username = trim(jq("#username").val());
                        var status = trim(jq("input[name=\"status\"]:checked").val());
                        var password = trim(jq("#password").val());
                        var repeatPassword = trim(jq("#repeat-password").val());
                        if(isNull(username)){
                            layer.msg("请填写账号",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        if(username.length < 6 || username.length > 16){
                            layer.msg("账号不能小于6位或不能大于16位字符",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return ;
                        }
                        var reg = /^[0-9a-zA-Z_]*$/;
                        if(!reg.test(username)){
                            layer.msg("账号只能是数字大小写字母下划线组成",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        if(isNull(password)){
                            layer.msg("请输入密码",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        if(password.length < 6 || password.length > 16){
                            layer.msg("密码不能小于6位或大于16位字符",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        var reg = /^[0-9a-zA-Z]*$/;
                        if(!reg.test(password)){
                            layer.msg("密码只能是数字大小写字母组成",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        if(password != repeatPassword){
                            layer.msg("两次密码不一致",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return;
                        }
                        jq("#username").val("");
                        jq("#password").val("");
                        jq("#repeat-password").val("");
                        var index = layer.load(1);
                        jq.ajax({
                            url : "/backend-admin/add-admin" ,
                            data : { username : username , password : password , status : status } ,
                            type : "POST" ,
                            dataType : "json" ,
                            success : function( data ){
                                layer.close(index);
                                if( data.code == "success" ){
                                    layer.msg("添加成功" ,{
                                        icon : 1 ,
                                        time : 1000 ,
                                    });
                                    setTimeout("window.location.reload()" , 1100);
                                }
                                if( data.code == "error" ){
                                    layer.msg( data.msg , {
                                        icon : 2 ,
                                        time : 2000 ,
                                    });
                                    return;
                                }
                            },//success end
                        });
                    });
                    //添加 end
                });
                jq("input[name=\"status\"]").iCheck({
                    radioClass : "iradio_square-blue",
                });
JS;
                $this->registerJs( $js );
                ?>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
jq(document).ready(function(){
    //选择每页显示条数刷新页面 start
    jq("#limit").change(function(){
        jq("#search").trigger("click");
    });
    //选择每页显示条数刷新页面 end
    //填充搜索条件 start
    var searchStr = window.location.search;
    if( searchStr.length > 0 ){
        searchStr = searchStr.substr(1,searchStr.length);
        var searchArr = searchStr.split("&");
        for( var i = 0 ; i < searchArr.length ; i++ ){
            var _tmp = searchArr[i].split("=");
            jq("#" + _tmp[0]).val(_tmp[1]);
        }
    }
    //填充搜索条件 end
    //搜索 start
    jq("#search").click(function(){
        var param = "";
        jq(".search-param").each(function(){
            param += jq(this).prop("id") + "=" + jq(this).val() + "&";
        });
        param = "?" + param.substr(0,param.length - 1);
        window.location.href = param;
    });
    //搜索 end
    //重置 start
    jq("#reset").click(function(){
        window.location.href = window.location.pathname;
    });
    //重置 end
    //时间选择 start
    jq("#create-begin-time,#create-end-time").datepicker({
        format : "yyyy-mm-dd",
        language : "zh-CN"
    });
    //时间选择 end
    //分页 start
    var count = jq("#count").text();
    var pages = jq("#pages").text();
    var pageSize = getQueryParams("limit");
    var offset = getQueryParams("offset");
    if(isNull(pageSize)){
        pageSize = jq("#default-page-size").val();
    }
    if(isNull(offset)){
        offset = 0 ;
    }
    var passPage = Math.ceil(offset/pageSize);
    var pageNumber = passPage + 1;
    jq("#pagination").pagination({
        dataSource : function(done){
            var result = [];
            for( var i = 1 ; i <= count ; i++ ){
                result.push(i);
            }
            done(result);
        },
        pageSize : pageSize,
        pageNumber : pageNumber,
        pageRange : 5,
        showGoInput : true ,
        showGoButton : true ,
        className : "paginationjs-theme-blue pull-right",
        callback : function( data , pagination ){
            var limit = pagination.pageSize;
            var currentPage = pagination.pageNumber;
            jq("#current-page").text(currentPage);
            var offset = ( currentPage - 1 ) * limit;
            //search param
            var param = "";
            jq(".search-param").each(function(){
                param += jq(this).prop("id") + "=" + jq(this).val() + "&";
            });
            param = "?" + param.substr(0,param.length - 1) + "&offset=" + offset;
            //切换分页同时修改url地址。防止刷新跳回原始页
            if( !!(window.history && history.pushState) ){
                history.pushState( null , "" , param );
            }
            param = "/backend-admin/ajax-query-admin-page" + param;
            //ajax query
            jq.ajax({
                url : param ,
                dataType : "html" ,
                type : "GET" ,
                success : function(data){
                    jq("#ajax-query-container").html(data);
                },//success end
            });
        },//callback end
    });
    //分页 end
    //打开添加管理员页面 start
    jq("#open-add-page").click(function(){
        jq("#add-admin").modal({
            backdrop : false ,
        });
    });
    //打开添加管理员页面 end
});
JS;
$this->registerJs( $js );
?>