<?php
/*ajax query admin page*/
?>
<?php if ( empty( $admins ) ) : ?>
    <div class="row">
        <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                没有数据
            </p>
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <input type="hidden" id="limit" value="<?= $limit ?>">
        <input type="hidden" id="offset" value="<?= $offset ?>">
        <div class="col-xs-12">
            <table class="table table-bordered table-hover dataTable" id="admins-list"
                   role="grid" aria-describedby="admins-info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc">账号</th>
                    <th class="sorting_asc">状态</th>
                    <th class="sorting_asc">添加时间</th>
                    <th class="sorting_asc">更新时间</th>
                    <th class="sorting_asc">操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ( $admins as $admin ) : ?>
                    <tr role="row">
                        <td>
                            <?= $admin[ 'username' ] ?>
                        </td>
                        <td data-id="<?= $admin[ 'id' ] ?>">
                            <?php if ( $admin[ 'status' ] == 0 ) : ?>
                                <button id="_disable" type="button"
                                        class="btn btn-xs btn-flat btn-default status-action"
                                        data-action="enable">禁用
                                </button>
                            <?php elseif ( $admin[ 'status' ] == 1 ) : ?>
                                <button id="enable" type="button" class="btn btn-xs btn-flat btn-success status-action"
                                        data-action="disable">启用
                                </button>
                            <?php else : ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?= date( 'Y-m-d H:i:s' , $admin[ 'register_time' ] ) ?>
                        </td>
                        <td>
                            <?= date( 'Y-m-d H:i:s' , $admin[ 'update_time' ] ) ?>
                        </td>
                        <td>
                            <button class="btn btn-info btn-xs action-btn actions no-margin no-padding"
                                    data-action="detail"
                                    data-id="<?= $admin[ 'id' ] ?>"><span class="fa fa-eye"></span>详情
                            </button>
                            <button class="btn btn-primary btn-xs action-btn actions no-margin no-padding"
                                    data-action="edit" data-id="<?= $admin[ 'id' ] ?>"><span class="fa fa-edit"></span>编辑
                            </button>
                            <button class="btn btn-danger btn-xs action-btn actions no-margin no-padding"
                                    data-action="delete" data-id="<?= $admin[ 'id' ] ?>"><span
                                        class="fa fa-trash"></span>删除
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif ?>
<?php
$js = <<<JS
jq(document).ready(function(){
    //切换状态 start
    jq(".status-action").click(function(){
        var action = jq(this).attr("data-action");
        var dataAction = jq(this).attr("data-action");
        var id = jq(this).parent().attr("data-id");
        var offset = jq("#offset").val();
        var limit = jq("#limit").val();
        if( dataAction == "disable" ){
            status = 0;
        }
        if( dataAction == "enable" ){
            status = 1;
        }
        var index = layer.load(1);
        jq.ajax({
            url : "/backend-admin/toggle-admin-status" ,
            data : { status : status , id : id } ,
            dataType : "json" ,
            type : "POST" ,
            success : function( data ){
                layer.close(index);
                if( data.code == "success" ){
                    layer.msg("操作成功",{
                        icon : 1 ,
                        time : 1000 ,
                    });
                    setTimeout(function(){
                        var searchStr = window.location.search;
                        var offset = jq("#offset").val();
                        if(searchStr.match(/offset/)){
                            window.location.href = replaceParamVal("offset",offset);
                        }else{
                            if(searchStr.length == 0){
                                url = "?offset=" + offset;
                            }else{
                                url = searchStr + "&offset=" + offset;
                            }
                            window.location.href = url;
                        }
                    },1100);
                }
                if( data.code == "error" ){
                    layer.msg(data.msg,{
                        icon : 2 ,
                        time : 1000 ,
                    });
                }
            },//success end
        });
    });
    //切换状态 end
    //操作 start
    jq(".actions").click(function(){
        var action = jq(this).attr("data-action");
        var id = jq(this).attr("data-id");
        //详情
        if( action == "detail" ){
            layer.open({
                type : 2 ,
                title : "查看详情" ,
                shadeClose : true ,
                shade : 0.8 ,
                area : ["700px","600px"] ,
                content : "/backend-admin/detail-admin?id=" + id ,
            });
        }
        //编辑
        if( action == "edit" ){
            layer.open({
                type : 2 ,
                title : "编辑" ,
                shadeClose : false ,
                shade : 0.8 ,
                area : ["300px","400px"],
                content : "/backend-admin/edit-admin?id=" + id ,
            });
        }
        //删除
        if( action == "delete" ){
            layer.msg("确定删除?",{
                btn : ["确定","取消"] ,
                time : 0 ,
                yes : function( index ){
                    layer.close( index );
                    var _index = layer.load(1);
                    jq.ajax({
                        url : "/backend-admin/delete-admin" ,
                        data : { id : id } ,
                        type : "POST" ,
                        dataType : "json" ,
                        success : function( data ){
                            layer.close( _index );
                            var offset = jq("#offset").val();
                            if( data.code == "success" ){
                                layer.msg("删除成功" , {
                                    icon : 1 ,
                                    time : 1000 ,
                                });
                                iReloadPage( offset , 1100 );
                            }
                        },//success end
                    });
                },//yes end
            });
        }
    });
    //操作 end
});
JS;
$this->registerJs( $js );
?>