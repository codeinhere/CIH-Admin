<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
?>
<?php if ( empty( $admin ) ): ?>
    <p>没有数据</p>
<?php else : ?>
    <div class="row">
        <input type="hidden" id="edit-id" value="<?= $admin[ 'id' ] ?>">
        <div class="col-xs-12">
            <div class="box-body">
                <div class="form">
                    <div class="form-group">
                        <label>管理员账号</label>
                        <input type="text" id="username" class="form-control" value="<?= $admin[ 'username' ] ?>">
                    </div>
                    <div class="form-group">
                        <label>密码</label>
                        <input type="password" id="password" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>重复密码</label>
                        <input type="password" id="repeat-password" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                        <br/>
                        <?php if ( $admin[ 'status' ] == 1 ) : ?>
                            启用<input type="radio" name="status" value="1" checked>
                            禁用<input type="radio" name="status" value="0">
                        <?php else : ?>
                            启用<input type="radio" name="status" value="1">
                            禁用<input type="radio" name="status" value="0" checked>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-flat btn-sm" id="edit-save">保存</button>
                        <button type="button" class="btn btn-default btn-flat btn-sm" id="edit-cancel">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $js = <<<JS
    jq(document).ready(function(){
        //取消
        jq("#edit-cancel").on("click",function(){
            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
            parent.layer.close(index);
            return true;
        });
        //取消
        
        //保存
        jq("#edit-save").on("click" , function(){
            var id = jq("#edit-id").val();
            var username = trim(jq("#username").val());
            var password = trim(jq("#password").val());
            var repeatPassword = trim(jq("#repeat-password").val());
            var status = jq("input[name=\"status\"]:checked").val();
            if(trim(username).length <= 0){
                layer.msg("请填写账号" , {
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            if( username.length > 16 ){
                layer.msg("账号不能超过16个字符",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            if(trim(password).length > 0){
                if( password.length < 6 ){
                    layer.msg("密码长度不能小于6个字符",{
                        icon : 2 ,
                        time : 2000 ,
                    });
                    return ;
                }
                if( password.length > 16 ){
                    layer.msg("密码长度不能超过16个字符",{
                        icon : 2 ,
                        time : 2000 ,
                    });
                    return ;
                }
                var preg = /^[0-9a-zA-Z]*$/;
                if(!preg.test(password)){
                //if( !password.test(/^[0-9a-zA-Z_]*$/)){
                    layer.msg("密码只能是大小写字母数字下划线组成",{
                        icon : 2 ,
                        time : 2000 ,
                    });
                    return ;
                }
            }
            if( password != repeatPassword ){
                layer.msg("两次密码不一致",{
                    icon : 2,
                    time : 2000 ,
                });
                return ;
            }
            if( isNull(status) ){
                layer.msg("请选择状态",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            jq("#username,#password,#repeat-password").val("");
            var index = layer.load(1);
            jq.ajax({
                url : "edit-admin" ,
                data : { id : id , username : username , password : password , repeatPassword : repeatPassword , status : status } ,
                dataType : "json" ,
                type : "POST" ,
                success : function(data){
                    layer.close(index);
                    if(data.code == "success" ){
                        layer.msg("保存成功",{
                            icon : 1 ,
                            time : 1000 ,
                        });
                        var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                        setTimeout("parent.layer.close(" + index + ");window.location.reload()" , 1100);
                        return ;
                    }
                    if(data.code == "error" ){
                        layer.msg( data.msg , {
                            icon : 2 ,
                            time : 2000 ,
                        });
                    }
                },//success end
            });
        });
        //保存
        //icheck start
        jq("input[name=\"status\"]").iCheck({
            radioClass : "iradio_square-blue" ,
        });
        //icheck end
    });
    jq("input[name=\"status\"]").iCheck({
        radioClass : "iradio_square-blue" ,
    });
JS;
    $this->registerJs( $js );
    ?>
<?php endif; ?>