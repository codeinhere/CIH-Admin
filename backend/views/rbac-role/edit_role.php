<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
?>
<?php if ( empty( $role ) ) : ?>
    <p>没有数据</p>
<?php else : ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body">
                <input type="hidden" id="id" value="<?= $role[ 'id' ] ?>">
                <div class="form">
                    <div class="form-group">
                        <label>名称</label>
                        <input id="role_name" type="text" class="form-control" value="<?= $role[ 'role_name' ] ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>状态</label>
                    <br/>
                    <?php if ( $role[ 'status' ] == 1 ) : ?>
                        启用<input type="radio" name="status" value="1" checked="checked">
                        禁用<input type="radio" name="status" value="0">
                    <?php else : ?>
                        启用<input type="radio" name="status" value="1">
                        禁用<input type="radio" name="status" value="0" checked="checked">
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary btn-flat btn-sm" id="save">保存</button>
                    <button type="button" class="btn btn-default btn-flat btn-sm" id="cancel">取消</button>
                </div>
            </div>
        </div>
    </div>
    <?php
    $js = <<<JS
        jq(document).ready(function(){
            jq("input[name=\"status\"]").iCheck({
                radioClass : "iradio_square-blue" ,
            });
            jq("#cancel").on("click" , function(){
                var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                parent.layer.close(index);
                return true;
            });
            jq("#save").on("click" , function(){
                var name = jq("#role_name").val()
                var status = jq("input[name=\"status\"]:checked").val();
                var id = jq("#id").val();
                var index = layer.load(1);
                var offset = jq("#offset").val();
                jq.ajax({
                    url : "edit-role" ,
                    data : { id : id , name : name , status : status } ,
                    dataType : "json" ,
                    type : "POST" ,
                    success : function(data){
                        layer.close(index);
                        layer.closeAll();
                        if( data.code == "success" ){
                            layer.msg("保存成功",{
                                icon : 1 ,
                                time : 1000 ,
                            });
                            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                            setTimeout("parent.layer.close(" + index + ");parent.window.location.reload()" , 1100);
                            return ;
                        }
                        if( data.code == "error" ){
                            layer.msg( data.msg , {
                                icon : 2 ,
                                time : 2000 ,
                            });
                        }
                    },//success end
                });
            });
        });
JS;
    $this->registerJs( $js );
    ?>
<?php endif; ?>