<?php
/* @var $this \yii\web\View */
/* @var $content string */
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register( $this );
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" style="height: auto;min-height:100%;">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode( $this->title ) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini wysihtml5-supported" style="height: auto; min-height: 100%;">
<?php $this->beginBody() ?>
<div class="wrapper" style="height:auto;min-height: 100%;">
    <header class="main-header">
        <a href="#" class="logo">
            <span class="logo-mini">CIH</span>
            <span class="logo-lg">
                CIH-Admin
            </span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only"></span>
            </a>
            <div class="navbar-customer-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown user user-menu">
                        <a class="admin-menu" href="#" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <span class="fa fa-user"></span>
                            <?= \Yii::$app->redis->hmget( 'admin_info' , 'admin_username' )[ 0 ] ?>
                            <span class="fa fa-caret-down"></span>
                        </a>
                        <ul class="dropdown-menu" style="left :-20px">
                            <li><a href="/web/logout"><span class="fa fa-sign-out"></span>退出</a></li>
                            <li><a href="#"><span class="fa fa-home"></span>设置</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <?= $this->render( 'left-menu' ); ?>
    </aside>
    <div class="content-wrapper">
        <?= $content; ?>
    </div>
    <footer class="main-footer"></footer>
</div>
<?php
$this->registerJs( '
jq(document).ready(function(){
    jq(".admin-menu").dropdown();
});
' );
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
