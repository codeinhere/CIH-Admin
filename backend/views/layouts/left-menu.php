<!--
<ul class="sidebar-menu tree" data-widget="tree">
    <li class="treeview menu-open">
        <a href="#">
            <i class="fa fa-cogs"></i>
            <span class="">权限控制</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu" style="display: block;">
            <li class=""><a href="/backend-admin/admin"><span class="fa fa-circle-o"></span>管理员管理</a></li>
            <li class=""><a href="/rbac-role/role"><span class="fa fa-circle-o"></span>角色管理</a></li>
            <li class=""><a href="/rbac-permission/permission"><span class="fa fa-circle-o"></span>权限管理</a></li>
            <li class=""><a href="/rbac-role-permission/set-role-permission"><span class="fa fa-circle-o"></span>分配角色权限</a>
            <li class=""><a href="/rbac-admin-permission/set-admin-permission"><span class="fa fa-circle-o"></span>分配管理员权限</a>
            <li class=""><a href="/rbac-menu/set-admin-menu"><span class="fa fa-circle-o"></span>菜单管理</a>
            </li>
        </ul>
    </li>
</ul>
-->
<?php
use common\models\RbacMenuModel;

$adminId = \Yii::$app->redis->hmget( 'admin_info' , 'admin_id' );
$adminId = $adminId[ 0 ];
$menus = RbacMenuModel::getMenusTreeByAdminId( $adminId );
$treeMenuId = \Yii::$app->redis->get( $adminId . '_tree_menu_id' );
?>
<?php if ( !empty( $menus ) ) : ?>
    <input type="hidden" id="tree-menu-id" value="<?= $treeMenuId ?>">
    <?php foreach ( $menus as $menu ) : ?>
        <ul class="sidebar-menu tree" data-widget="tree">
            <!--父级菜单就是导航-->
            <?php if ( $menu[ 'permission_id' ] != 0 ) : ?>
                <li class="tree-menu-li" data-menu-id="<?= $menu[ 'id' ] ?>">
                    <a href="/<?= $menu[ 'permission' ][ 'route_controller' ] ?>/<?= $menu[ 'permission' ][ 'route_action' ] ?>">
                        <i class="<?= empty( $childMenusTree[ 'icon' ] ) ? 'fa fa-circle-o' : $childMenusTree[ 'icon' ] ?>"></i>
                        <span><?= $menu[ 'menu_name' ] ?></span>
                    </a>
                </li>
                <!--父级菜单不是导航-->
            <?php else : ?>
                <li class="treeview">
                    <!--父级菜单-->
                    <a href="#">
                        <i class="<?= empty( $menu[ 'icon' ] ) ? 'fa fa-circle-o' : $menu[ 'icon' ] ?>"></i>
                        <span><?= $menu[ 'menu_name' ] ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none">
                        <!--循环输出子菜单-->
                        <?php foreach ( $menu[ 'childMenusTree' ] as $childMenusTree ) : ?>
                            <li class="tree-menu-li" data-menu-id="<?= $childMenusTree[ 'id' ] ?>">
                                <a class="tree-href"
                                   data-href="/<?= $childMenusTree[ 'permission' ][ 'route_controller' ] ?>/<?= $childMenusTree[ 'permission' ][ 'route_action' ] ?>"
                                   href="/<?= $childMenusTree[ 'permission' ][ 'route_controller' ] ?>/<?= $childMenusTree[ 'permission' ][ 'route_action' ] ?>">
                                    <!--图标-->
                                    <span class="<?= empty( $childMenusTree[ 'icon' ] ) ? 'fa fa-circle-o' : $childMenusTree[ 'icon' ] ?>"></span>
                                    <!--菜单名称--><?= $childMenusTree[ 'menu_name' ] ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    <?php endforeach; ?>
    <?php
    $js = <<<JS
    jq(document).ready(function(){
        var currentTreeMenuId = jq("#tree-menu-id").val();
        jq(".tree-menu-li").each(function(){
            var dataMenuId = jq(this).attr("data-menu-id");
            if(dataMenuId == currentTreeMenuId){
                jq(this).addClass("active");
                jq(this).parent().css({"display":"block"});
                jq(this).parent().parent().addClass("menu-open");
            }
        });
    });
JS;
    $this->registerJs( $js );
    ?>
<?php else : ?>
<?php endif; ?>
