<!DOCTYPE html>
<html lang="en">
<meta charset="utf8">
<head>
    <title>登录</title>
    <link rel="stylesheet" href="/css/adminlte/bower_components/bootstrap/dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="/css/adminlte/dist/css/AdminLTE.css"/>
    <script src="/css/adminlte/bower_components/jquery/dist/jquery.js"></script>
    <script src="/js/layer/layer.js"></script>
</head>
<body class="hold-transition login-page">
<input type="hidden" id="default-route" value="<?= $defaultRoute ?>">
<div class="login-box">
    <div class="login-box-body">
        <p class="login-box-msg">登录管理后台</p>
        <div class="form-group has-feedback">
            <input id="username" class="form-control" placeholder="管理员账号" type="text">
        </div>
        <div class="form-group has-feedback">
            <input id="password" class="form-control" placeholder="管理员密码" type="password">
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <input type="text" class="form-control" id="code" placeholder="验证码">
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <img src="<?= $verifyCodeImage ?>" id="refresh-code" class="form-control"
                         style="border:0px;width:100px;cursor:pointer"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button id="login" type="button" class="btn btn-primary btn-block btn-flat">登录</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    var jq = $;

    jq(document).ready(function () {
        //默认路由
        var defaultRoute = jq("#default-route").val();
        //登录 start
        jq("#login").click(function () {
            var username = jq("#username").val();
            var password = jq("#password").val();
            var code = jq("#code").val();
            if (username.length == 0) {
                layer.tips('请输入账号', '#username');
                return;
            }
            if (password.length == 0) {
                layer.tips('请输入密码', '#password');
                return;
            }
            if (code.length == 0) {
                layer.tips("请输入验证码", '#code')
                return;
            }
            jq("#username,#password").val("");
            var _index = layer.load(1);
            jq.ajax({
                url: "/web/login",
                data: {username: username, password: password, code: code},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    console.log(data);
                    layer.close(_index);
                    if (data.code == 'fail') {
                        layer.msg(data.msg, {
                            icon: 2,
                            time: 1000,
                        });
                        setTimeout("window.location.reload()", 1100);
                        return;
                    }
                    if (data.code == 'success') {
                        layer.msg(data.msg, {
                            icon: 1,
                            time: 1000,
                        });
                        setTimeout("window.location.href=\"/" + defaultRoute + "\"", 1100);
                    }
                },
            });
        });
        //登录 end
        //刷新验证码图片 start
        jq("#refresh-code").on("click", function () {
            jq.ajax({
                url: "/web/verify-code-image",
                data: {},
                dataType: "text",
                type: "GET",
                success: function (data) {
                    var _src = data + "?rnd=" + Math.random()
                    jq("#refresh-code").attr("src", _src);
                }
            });
        });
        //刷新验证码图片 end
    });
</script>
