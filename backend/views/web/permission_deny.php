<section class="content">
    <div class="error-page">
        <h2 class="headline text-danger">403</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-danger"></i>You have no permission to request this page.</h3>

            <p>
                You have <b class="text-yellow">NO PERMISSION</b> to request this page.Please contact with your
                administrator.<a href="javascript:history.back()">Click to go back</a>
            </p>

        </div>
        <!-- /.error-content -->
    </div>
</section>
