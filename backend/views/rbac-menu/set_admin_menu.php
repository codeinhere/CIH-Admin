<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->title = '设置管理员菜单';
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
    <input type="hidden" id="admin-id" value="<?= $adminId ?>">
    <input type="hidden" id="admin-username" value="<?= $adminUsername ?>">
    <section class="content-header">
        <h1>所有菜单
            <small>管理所有菜单</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/"><i class="fa fa-dashboard"></i>首页</a>
            </li>
            <li class="active">
                <?= $this->title ?>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="admins-data-table" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-xs-6">
                                    选择管理员:
                                    <select aria-hidden="true" tabindex="-1" id="select-admin"
                                            class="select2-hidden-accessible"
                                            style="width: 100%;" name="username">
                                        <option value="<?= $adminId ?>"
                                                selected="selected"><?= $adminUsername ?></option>

                                    </select>
                                </div>
                                <div class="col-xs-6 pull-right">
                                    <button class="btn btn-primary btn-sm pull-right" id="open-add-page">添加菜单</button>
                                </div>
                            </div>

                            <div id="ajax-query-container">
                                <?php if ( empty( $menus ) ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                没有数据
                                            </p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <?php foreach ( $menus as $menu ) : ?>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="box box-solid">
                                                    <div class="box-body">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <?php if ( empty( $menu[ 'permission' ] ) ) : ?>
                                                                    <div class="pull-left">
                                                                        父级菜单:<b><?= $menu[ 'menu_name' ] ?></b>
                                                                        |是否可见:
                                                                        <?php if ( $menu[ 'visible' ] == 1 ) : ?>
                                                                            <label class="label label-success no-padding">可见</label>
                                                                        <?php else : ?>
                                                                            <label class="label label-warning no-padding">不可见</label>
                                                                        <?php endif; ?>
                                                                        |状态:
                                                                        <?php if ( $menu[ 'status' ] == 1 ) : ?>
                                                                            <label class="label label-success no-padding">正常</label>
                                                                        <?php else : ?>
                                                                            <label class="label label-warning no-padding">禁用</label>
                                                                        <?php endif; ?>
                                                                        |排序:<?= $menu[ 'sort' ] ?>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="javascript:void(0);"
                                                                           class="btn btn-link no-padding no-margin action-edit-parent-menu"
                                                                           data-id="<?= $menu[ 'id' ] ?>"
                                                                           data-parent="false"
                                                                           title="编辑"><span
                                                                                    class="fa fa-edit text-info"></span>
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                           class="btn btn-link no-padding no-margin text-danger delete-parent"
                                                                           data-id="<?= $menu[ 'id' ] ?>"
                                                                           title="删除"><span
                                                                                    class="fa fa-trash text-danger"></span>
                                                                        </a>
                                                                    </div>
                                                                <?php else : ?>
                                                                    <div class="pull-left">
                                                                        父级菜单:<b><?= $menu[ 'menu_name' ] ?></b>
                                                                        |权限:
                                                                        <b><?= $menu[ 'permission' ][ 'permission_name' ] ?></b>
                                                                        |路由:
                                                                        <b><?= $menu[ 'permission' ][ 'route_controller' ] ?>
                                                                            /<?= $menu[ 'permission' ][ 'route_action' ] ?></b>
                                                                        |是否可见:
                                                                        <?php if ( $menu[ 'visible' ] == 1 ) : ?>
                                                                            <label class="label label-success no-padding">可见</label>
                                                                        <?php else : ?>
                                                                            <label class="label label-warning no-padding">不可见</label>
                                                                        <?php endif; ?>
                                                                        |状态:
                                                                        <?php if ( $menu[ 'status' ] == 1 ) : ?>
                                                                            <label class="label label-success no-padding">正常</label>
                                                                        <?php else : ?>
                                                                            <label class="label label-warning no-padding">禁用</label>
                                                                        <?php endif; ?>
                                                                        |排序:<?= $menu[ 'sort' ] ?>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="javascript:void(0);"
                                                                           class="btn btn-link no-padding no-margin action-edit-parent-menu"
                                                                           data-id="<?= $menu[ 'id' ] ?>"
                                                                           data-parent="true"

                                                                           title="编辑"><span
                                                                                    class="fa fa-edit text-info"></span>
                                                                        </a>
                                                                        <a href="javascript:void(0);"
                                                                           class="btn btn-link no-padding no-margin text-danger delete-parent"
                                                                           data-id="<?= $menu[ 'id' ] ?>"
                                                                           title="删除"><span
                                                                                    class="fa fa-trash text-danger"></span>
                                                                        </a>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <!--
                                                                <div class="pull-right">
                                                                    <a href="javascript:void(0);"
                                                                       class="btn btn-link no-padding no-margin action-edit-parent-menu"
                                                                       data-id="<?= $menu[ 'id' ] ?>"
                                                                       title="编辑"><span
                                                                                class="fa fa-edit text-info"></span>
                                                                    </a>
                                                                    <a href="javascript:void(0);"
                                                                       class="btn btn-link no-padding no-margin text-danger delete-parent"
                                                                       data-id="<?= $menu[ 'id' ] ?>"
                                                                       title="删除"><span
                                                                                class="fa fa-trash text-danger"></span>
                                                                    </a>
                                                                </div>
                                                                -->
                                                                <?php if ( empty( $menu[ 'childMenus' ] ) ) : ?>
                                                                    <br/>
                                                                    <ul>
                                                                        <li>
                                                                            <i>
                                                                                <small>没有子级菜单</small>
                                                                            </i>
                                                                        </li>
                                                                    </ul>
                                                                <?php else : ?>
                                                                    <ul>
                                                                        <table class="table-bordered table table-hover">
                                                                            <thead>
                                                                            <th>名称</th>
                                                                            <th>权限</th>
                                                                            <th>路由</th>
                                                                            <th>是否可见</th>
                                                                            <th>状态</th>
                                                                            <th>排序</th>
                                                                            <th>操作</th>
                                                                            </thead>
                                                                            <tbody>
                                                                            <?php foreach ( $menu[ 'childMenus' ] as $childMenu ) : ?>
                                                                                <tr>
                                                                                    <li style="list-style: none">
                                                                                        <td>
                                                                                            <input type="checkbox"
                                                                                                   class="menu-checkbox"> <?= $childMenu[ 'menu_name' ] ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if ( empty( $childMenu[ 'permission' ][ 'permission_name' ] ) ) : ?>
                                                                                                <label class="label label-warning">没有权限</label>
                                                                                            <?php else : ?>
                                                                                                <?= $childMenu[ 'permission' ][ 'permission_name' ] ?>
                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if ( empty( $childMenu[ 'permission' ] ) ) : ?>
                                                                                                <label class="label label-warning">没有路由</label>
                                                                                            <?php else : ?>
                                                                                                <?= $childMenu[ 'permission' ][ 'route_controller' ] ?>/<?= $childMenu[ 'permission' ][ 'route_action' ] ?>
                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if ( $childMenu[ 'visible' ] == 1 ) : ?>
                                                                                                <label class="label label-success">可见</label>
                                                                                            <?php else : ?>
                                                                                                <label class="label label-warning">不可见</label>
                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if ( $childMenu[ 'status' ] ) : ?>
                                                                                                <label class="label label-success">正常</label>
                                                                                            <?php else : ?>
                                                                                                <label class="label label-warning">禁用</label>
                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?= $childMenu[ 'sort' ] ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <a href="javascript:void(0)"
                                                                                               class="btn btn-link no-padding no-margin action-edit"
                                                                                               data-id="<?= $childMenu[ 'id' ] ?>"
                                                                                               title="编辑"><span
                                                                                                        class="fa fa-edit text-info"></span>
                                                                                            </a>
                                                                                            <a href="javascript:void(0);"
                                                                                               class="btn btn-link no-padding no-margin action-delete"
                                                                                               data-id="<?= $childMenu[ 'id' ] ?>"
                                                                                               title="删除"><span
                                                                                                        class="fa fa-trash text-danger"></span>
                                                                                            </a>
                                                                                        </td>
                                                                                    </li>
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php
                                    $js = <<<JS
                                //ajax query admin menu start
                                function ajaxQueryAdminMenu() {
                                    var adminId = jq("#admin-id").val();
                                    jq.ajax({
                                        url : "/rbac-menu/ajax-query-admin-menu" ,
                                        data : { adminId : adminId } ,
                                        dataType : "html" ,
                                        type : "GET" ,
                                        success : function( data ){
                                            jq("#ajax-query-container",parent.document).html(data);
                                        },//success end
                                    });
                                }
                                //ajax query admin menu end
     
                                jq(document).ready(function(){
                                    
                                   //icheck start
                                   jq(".menu-checkbox").iCheck({
                                        checkboxClass : "icheckbox_minimal-blue",
                                   });
                                   //icheck end
                                   //编辑父级菜单 start
                                   jq(".action-edit-parent-menu").on("click",function(){
                                       var adminId = jq("#admin-id").val();
                                       var id = jq(this).attr("data-id");
                                       var isParent = jq(this).attr("data-parent");
                                       layer.open({
                                           type : 2 ,
                                           title : "编辑" ,
                                           shareClose : true ,
                                           shade : 0.8 ,
                                           area : [ "300px" , "450px" ],
                                           content : "/rbac-menu/edit-parent-menu?id=" + id + "&adminId=" + adminId + "&isParent=" + isParent
                                        });
                                   });
                                   //编辑父级菜单 end
                                   //编辑 start
                                   jq(".action-edit").on("click",function(){
                                       var adminId = jq("#admin-id").val();
                                       var id = jq(this).attr("data-id");
                                       layer.open({
                                           type : 2 ,
                                           title : "编辑" ,
                                           shareClose : true ,
                                           shade : 0.8 ,
                                           area : [ "300px" , "500px" ],
                                           content : "/rbac-menu/edit-menu?id=" + id + "&adminId=" + adminId,
                                        });
                                   });
                                   //编辑 end
                                   //删除 start
                                   jq(".action-delete").on("click",function(){
                                       var id = jq(this).attr("data-id");
                                       var adminId = jq("#admin-id").val();
                                       layer.msg("确定删除?",{
                                           time : 0 ,
                                           btn : ["确定","取消"] ,
                                           yes : function(index){
                                               layer.close(index);
                                               var _index = layer.load(1);
                                               jq.ajax({
                                                    url : "/rbac-menu/delete-menu" ,
                                                    data : { id : id , adminId : adminId } ,
                                                    dataType : "json" ,
                                                    type : "POST" ,
                                                    success : function( data ){
                                                        layer.close(_index);
                                                        if( data.code == "error" ){
                                                            layer.msg(data.msg,{
                                                                icon : 2 ,
                                                                time : 1000 ,
                                                            });
                                                        }
                                                        if( data.code == "success" ){
                                                            layer.msg("删除成功",{
                                                                icon : 1 ,
                                                                time : 1000 ,
                                                            });
                                                            //刷新列表
                                                            setTimeout("window.location.reload()" , 1100);
                                                        }
                                                    },//success end
                                               });
                                           },//yes end
                                       });
                                   });
                                   //删除 end
                                   //删除父级菜单 start
                                   jq(".delete-parent").on("click",function(){
                                       var id = jq(this).attr("data-id");
                                       var adminId = jq("#admin-id").val();
                                       layer.msg("一并删除子级菜单.确定删除?",{
                                           time : 0 ,
                                           btn : ["确定","取消"] ,
                                           yes : function(index){
                                               layer.close(index);
                                               var _index = layer.load(1);
                                               jq.ajax({
                                                    url : "/rbac-menu/delete-menu" ,
                                                    data : { id : id , adminId : adminId } ,
                                                    dataType : "json" ,
                                                    type : "POST" ,
                                                    success: function(data) {
                                                        layer.close(_index);
                                                        if( data.code == "error" ){
                                                            layer.msg( data.msg , {
                                                                icon : 2 ,
                                                                time : 1000 ,
                                                            });
                                                        }
                                                        if( data.code == "success" ){
                                                            layer.msg( "删除成功" , {
                                                                icon : 1 ,
                                                                time : 1000 ,
                                                            });
                                                            //刷新列表
                                                            setTimeout("window.location.reload()" , 1100);
                                                        }
                                                    },//success end
                                               });
                                           },//yes end
                                       });
                                   });
                                   //删除父级菜单 end
                                });
JS;
                                    $this->registerJs( $js );
                                    ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="add-menu" tabindex="-1" role="dialog" aria-labelledby="addAdminModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addAdminModalLabel">添加菜单</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>名称</label>
                        <input type="text" class="form-control" id="menu-name">
                    </div>
                    <div class="form-group">
                        <label>管理员</label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="modal-select-admin"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="admin" disabled>
                            <option value="<?= $adminId ?>" selected="selected"><?= $adminUsername ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>是否是父级菜单</label>
                        <br/>
                        是<input type="radio" name="isParent" value="1" checked>
                        否<input type="radio" name="isParent" value="0">
                    </div>
                    <div class="form-group" style="display: none" id="i-select-parent">
                        <label>父级菜单</label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="select-parent-menu"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="parent">
                            <option value="" selected="selected">选择父级菜单</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>权限</label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="select-permission"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="parent" disabled>
                            <option value="" selected="selected">父级菜单无需选择权限</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                        <br/>
                        可用<input type="radio" name="status" value="1" checked>
                        禁用<input type="radio" name="status" value="0">
                    </div>
                    <div class="form-group">
                        <label>是否隐藏</label>
                        <br/>
                        是<input type="radio" name="isVisible" value="1">
                        否<input type="radio" name="isVisible" value="1" checked>
                    </div>
                    <div class="form-group">
                        <label>排序</label>
                        <input type="text" class="form-control" id="sort">
                    </div>
                    <div class="form-group">
                        <label>菜单图标</label>
                        <input type="text" class="form-control" id="icon">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                    <button type="button" class="btn btn-primary" id="add">添加</button>
                </div>

            </div>
            <?php
            $js = <<<JS
        jq(document).ready(function(){
            jq.fn.modal.Constructor.prototype.enforceFocus = function () {};
            //var adminId = jq("#modal-select-admin").val();
            //切换父级菜单 start
            jq("input[name=\"isParent\"]").on("ifChecked",function(){
                if(jq(this).val() == "0"){
                    jq("#i-select-parent").css({"display":"block"});
                    jq("#select-permission").attr("disabled" , false);
                }else{
                    jq("#i-select-parent").css({"display":"none"});
                    jq("#select-permission").attr("disabled" , "disabled");
                }
            });
            //切换父级菜单 end
            //icheck start
            jq("input[name=\"isParent\"]").iCheck({
                radioClass : "iradio_square-blue",
            });
            jq("input[name=\"status\"]").iCheck({
                radioClass : "iradio_square-blue",
            });
            jq("input[name=\"isVisible\"]").iCheck({
                radioClass : "iradio_square-blue",
            });
            //icheck start
            //select2 start
            jq("#select-permission").select2({
                ajax : {
                    url : "/rbac-menu/ajax-search-permission" ,
                    dataType : "json" ,
                    type : "GET" ,
                    data : function( term , size ){
                        return {
                            search : term ,
                            size : 10 ,
                        };
                    },//data end
                    processResults : function( data ){
                        return {
                            results : data.msg ,
                        };
                    },//results end
                },
            });
            jq("#select-parent-menu").select2({
                ajax : {
                    url : "/rbac-menu/ajax-search-parent-menu" ,
                    dataType : "json" ,
                    type : "GET" ,
                    data : function( term , size ){
                        return {
                            search : term ,
                            size : 10 ,
                            adminId : jq("#modal-select-admin").val(),
                        };
                    },//data end
                    processResults : function( data ){
                        return {
                            results : data.msg ,
                        };
                    },//results end
                },
            });
            jq("#modal-select-admin").select2({});
            //select2 end
            //add start
            jq("#add").on("click",function(){
                //var adminId = jq("#admin-id").val();
                var adminId = jq("#modal-select-admin").val();
                var menuName = jq("#menu-name").val();
                var isParent = jq("input[name=\"isParent\"]:checked").val();
                var parentMenu = jq("#select-parent-menu").val();
                var permission = jq("#select-permission").val();
                var status = jq("input[name=\"status\"]:checked").val();
                var isVisible = jq("input[name=\"isVisible\"]:checked").val();
                var sort = trim(jq("#sort").val());
                var icon = trim(jq("#icon").val());
                if(isNull(adminId)){
                    layer.msg("请选择管理员",{
                        icon : 0 ,
                        time : 1000 ,
                    });
                    return ;
                }
                if(isNull(menuName)){
                    layer.msg("请填写名称",{
                        icon : 0 ,
                        time : 1000 ,
                    });
                    return ;
                }
                if(menuName.length > 20){
                    layer.msg("名称长度不能超过20个字",{
                        icon : 0 ,
                        time : 1000 ,
                    });
                    return ;
                }
                if(isParent == 0){
                    if(isNull(parentMenu)){
                        layer.msg("请选择父级菜单",{
                            icon : 0 ,
                            time : 1000 ,
                        });
                        return ;
                    }
                }
                if(isNull(status)){
                    layer.msg("请选择状态",{
                        icon : 0 ,
                        time : 1000 ,
                    });
                    return ;
                }
                if(isNull(isVisible)){
                    layer.msg("请选择是否隐藏",{
                        icon : 0 ,
                        time : 1000 ,
                    });
                    return ;
                }
                if(!isNull(sort)){
                    if(!isPositiveInteger(sort)){
                        layer.msg("排序只能是正整数",{
                            icon : 0 ,
                            time : 1000 ,
                        });
                        return ;
                    }
                }
                jq("#menu-name").val("");
                jq("#sort").val("");
                jq("#icon").val("");
                var index = layer.load(1);
                jq.ajax({
                    url : "/rbac-menu/add-admin-menu" ,
                    data : { "adminId" : adminId , "menuName" : menuName , "isParent" : isParent , "parentMenu" : parentMenu , "permission" : permission , "status" : status , "isVisible" : isVisible , "sort" : sort , "icon" : icon } ,
                    dataType : "json" ,
                    type : "POST" ,
                    success : function(data){
                        layer.close(index);
                        if( data.code == "error" ){
                            layer.msg( data.msg , {
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return ;
                        }
                        if( data.code == "success" ){
                            layer.msg( "添加成功" , {
                                icon : 1 ,
                                time : 1000 ,
                            });
                            setTimeout("jq(\"#close\").trigger(\"click\");window.location.reload()" , 1100);
                        }
                    },//success end
                });
            });
            //add end
        });
JS;
            $this->registerJs( $js );
            ?>
        </div>
    </div>
<?php
$js = <<<JS
jq(document).ready(function(){
    //select admin start
    jq("#select-admin").select2({
        ajax : {
            url : "/rbac-menu/set-admin-menu" ,
            dataType : "json" ,
            type : "GET" ,
            data : function( term , size ){
                return {
                    search : term ,
                    size : 10 ,
                };
            },//data end
            processResults : function( data ){
                return {
                    results : data.msg ,
                };
            },//results end
        },
    });
    //select admin end
    
    //change start
    jq("#select-admin").on("change",function(){
        var adminId = jq(this).val();
        var index = layer.load(1);
        window.location.href = 'set-admin-menu?adminId=' + adminId;
    });
    //change end
            
    //打开添加管理员页面 start
    jq("#open-add-page").click(function(){
        jq("#add-menu").modal({
            backdrop : false ,
        });
    });
    //打开添加管理员页面 end
});
JS;
$this->registerJs( $js );
?>