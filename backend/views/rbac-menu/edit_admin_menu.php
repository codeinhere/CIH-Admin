<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>

<?php if ( empty( $menu ) ) : ?>
    <p>没有数据</p>
<?php else : ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body">
                <input type="hidden" id="id" value="<?= $menu[ 'id' ] ?>">
                <input type="hidden" id="admin-id" value="<?= $adminId ?>">
                <div class="form">
                    <div class="form-group">
                        <label>名称</label>
                        <input id="menu_name" type="text" class="form-control" value="<?= $menu[ 'menu_name' ] ?>">
                    </div>
                    <div class="form-group">
                        <label>是否可见</label>
                        <br/>
                        <?php if ( $menu[ 'visible' ] == 1 ) : ?>
                            是<input type="radio" name="is_visible" value="1" checked>
                            否<input type="radio" name="is_visible" value="0">
                        <?php else : ?>
                            是<input type="radio" name="is_visible" value="1">
                            否<input type="radio" name="is_visible" value="0" checked>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label>状态是否可用</label>
                        <br/>
                        <?php if ( $menu[ 'status' ] == 1 ) : ?>
                            是<input type="radio" name="status" value="1" checked>
                            否<input type="radio" name="status" value="0">
                        <?php else : ?>
                            是<input type="radio" name="status" value="1">
                            否<input type="radio" name="status" value="0" checked>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label>父级菜单:
                            <small><?= $menu[ 'menu_name' ] ?></small>
                        </label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="select-parent-menu"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="parent" disabled>
                            <option value="<?= $menu[ 'id' ] ?>"
                                    selected="selected"><?= $menu[ 'menu_name' ] ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>权限:
                            <small><?= $menu[ 'permission' ][ 'permission_name' ] ?></small>
                        </label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="select-permission"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="parent">
                            <option value="" selected="selected">不修改则不选择</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>排序</label>
                        <input type="text" class="form-control" value="<?= $menu[ 'sort' ] ?>" id="sort">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-flat btn-sm" id="save">保存</button>
                        <button type="button" class="btn btn-default btn-flat btn-sm" id="cancel">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $js = <<<JS
    //ajax query admin menu start
    function ajaxQueryAdminMenu() {
        var adminId = jq("#admin-id").val();
        jq.ajax({
            url : "/rbac-menu/ajax-query-admin-menu" ,
            data : { adminId : adminId } ,
            dataType : "html" ,
            type : "GET" ,
            success : function( data ){
                jq("#ajax-query-container",parent.document).html(data);
            },//success end
        });
    }
    //ajax query admin menu end
        
    jq(document).ready(function(){
        jq.fn.modal.Constructor.prototype.enforceFocus = function () { };
        //select parent menu start
        jq("#select-parent-menu").select2({
            ajax : {
                url : "/rbac-menu/ajax-search-parent-menu" ,
                dataType : "json" ,
                type : "GET" ,
                data : function( term , size ){
                    return {
                        search : term ,
                        size : 10 ,
                    };
                },//data end
                processResults : function( data ){
                    return {
                        results : data.msg ,
                    };
                },//results end
            },
        });
        //select parent menu end
        //select permission start
        jq("#select-permission").select2({
            ajax : {
                url : "/rbac-menu/ajax-search-permission" ,
                dataType : "json" ,
                type : "GET" ,
                data : function( term , size ){
                    return {
                        search : term ,
                        size : 10 ,
                    };
                },//data end
                processResults : function( data ){
                    return {
                        results : data.msg ,
                    };
                },//results end
            },
        });
        //select permission end
        //icheck start
        jq("input[name=\"status\"]").iCheck({
            radioClass : "iradio_square-blue" ,
        });
        jq("input[name=\"is_visible\"]").iCheck({
            radioClass : "iradio_square-blue" ,
        });
        //icheck end
        
        //取消 start
        jq("#cancel").on("click",function(){
            var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
            parent.layer.close(index);
            return true;
        });
        //取消 end
        //保存 start
        jq("#save").on("click",function(){
            var id = jq("#id").val();
            var adminId = jq("#admin-id").val();
            var name = trim(jq("#menu_name").val());
            var isVisible = jq("input[name=\"is_visible\"]:checked").val();
            var status = jq("input[name=\"status\"]:checked").val();
            //var parentMenu = jq("#select-parent-menu").val();
            var permission = jq("#select-permission").val();
            var sort = trim(jq("#sort").val());
            if( name.length <= 0 ){
                layer.msg("请填写名称",{
                    icon : 2 ,
                    time : 1000 ,
                });
                return ;
            }
            if( sort.length > 0 ){
                if( !isNumber(sort) ){
                    layer.msg("排序只能是数字",{
                        icon : 2 ,
                        time : 1000 ,
                    });
                    return ;
                }
            }
            if( !isNumber( isVisible ) ){
                layer.msg( "发生错误.停止执行" , {
                    icon : 2 ,
                    time : 2000 ,
                } );
                return ;
            }
            if( !isNumber( status ) ){
                layer.msg( "发生错误.停止执行" , {
                    icon : 2 ,
                    time : 2000 ,
                } );
                return ;
            }
            jq("#menu_name").val("");
            jq("#sort").val("");
            var index = layer.load(1);
            jq.ajax({
                url : "/rbac-menu/edit-menu" ,
                data : { adminId : adminId , id : id , name : name , isVisible : isVisible , status : status , sort : sort , /*parentMenu : parentMenu ,*/ permission : permission } ,
                dataType : "json" ,
                type : "POST" ,
                success : function( data ){
                    layer.close( index );
                    if( data.code == "success" ){
                        layer.msg("修改成功" , {
                            icon : 1 ,
                            time : 1000 ,
                        });
                        var _index = parent.layer.getFrameIndex(window.name);
                        setTimeout("parent.layer.close(" + index + ");parent.window.location.reload()" , 1100);
                    }
                    if( data.code == "error" ){
                        layer.msg( data.msg , {
                            icon : 2 ,
                            time : 2000 ,
                        } );
                        return ;
                    }
                },//success end
            });
        });
        //保存 end
    });
JS;
    $this->registerJs( $js ) ?>
<?php endif; ?>