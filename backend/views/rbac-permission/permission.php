<?php
/**
 * 管理员管理页面
 */
use yii\helpers\Html;
use backend\assets\AppAsset;

$this->title = '权限管理';
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
    <section class="content-header">
        <h1>所有权限
            <small>管理所有权限</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/"><i class="fa fa-dashboard"></i>首页</a>
            </li>
            <li class="active">
                <?= $this->title ?>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div id="admins-data-table" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div id="page-length" class="dataTables_length">
                                        <label>每页显示
                                            <select id="limit" class="form-control input-sm" name="page-length"
                                                    aria-controls="example1">
                                                <option value="">显示所有权限</option>
                                            </select>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-6 pull-right">
                                    <button class="btn btn-primary btn-sm pull-right" id="open-add-page">添加权限</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box-body">
                                        <div class="form-group no-margin">
                                            <label class="control-label">权限名称:</label>
                                            <input type="text" id="permission_name"
                                                   class="form-control input-sm search-param"
                                                   style="width:100px;">
                                        </div>
                                        <div class="form-group no-margin">
                                            <label class="control-label">控制器:</label>
                                            <input type="text" id="controller_name"
                                                   class="form-control input-sm search-param">
                                        </div>
                                        <div class="form-group no-margin">
                                            <label class="control-label">方法:</label>
                                            <input type="text" id="action_name"
                                                   class="form-control input-sm search-param">
                                        </div>
                                        <div class="form-group no-margin">
                                            <button class="btn btn-primary btn-sm" id="search">搜索</button>
                                            <button class="btn btn-info btn-sm" id="reset">重置</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ajax-query-container">
                                <?php if ( empty( $permissions ) ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                没有数据
                                            </p>
                                        </div>
                                    </div>
                                <?php else : ?>
                                <?php foreach ( $permissions as $permission ) : ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-header">
                                                    <div class="pull-left">
                                                    <span>
                                                        <input class="permission-check parent-permission-check-all"
                                                               data-id="<?= $permission[ 'id' ] ?>"
                                                               type="checkbox">
                                                    </span>
                                                        父级权限:
                                                        <h3 class="box-title"><?= $permission[ 'permission_name' ] ?> </h3>
                                                        <a data-id="<?= $permission[ 'id' ] ?>"
                                                           href="javascript:void(0);"
                                                           class="text-danger delete-parent-permission">删除父权限</a>

                                                    </div>
                                                    <div class="pull-right">
                                                        <button data-id="<?= $permission[ 'id' ] ?>" type="button"
                                                                class="btn btn-danger btn-sm delete-btn">
                                                            批量删除
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="box-body table-responsive no-padding table-bordered">
                                                    <?php if ( !empty( $permission[ 'childPermissions' ] ) ) : ?>
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 20%;">
                                                                名称
                                                            </th>
                                                            <th style="width: 20%">
                                                                控制器
                                                            </th>
                                                            <th style="width: 20%">
                                                                方法
                                                            </th>
                                                            <th style="width: 20%;">
                                                                状态
                                                            </th>
                                                            <th style="width: 10%">
                                                                操作
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ( $permission[ 'childPermissions' ] as $childPermission ) : ?>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    <input data-id="<?= $childPermission[ "id" ] ?>"
                                                                           name="permission-checkbox-<?= $childPermission[ 'parent_id' ] ?>"
                                                                           class="permission-check permission-check-<?= $childPermission[ 'parent_id' ] ?>"
                                                                           type="checkbox">
                                                                    <?= $childPermission[ 'permission_name' ] ?>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <?= $childPermission[ 'route_controller' ] ?>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <?= $childPermission[ 'route_action' ] ?>
                                                                </td>
                                                                <td style="width: 20%;">
                                                                    <?php if ( $childPermission[ 'status' ] == 1 ) : ?>
                                                                        <label class="label label-success">正常</label>
                                                                    <?php else : ?>
                                                                        <label class="label label-warning">禁用</label>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td style="width: 10%;">
                                                                    <button class="btn btn-primary btn-xs i-edit-btn action-btn"
                                                                            data-id="<?= $childPermission[ 'id' ] ?>">
                                                                        <span class="fa fa-edit">编辑</span>
                                                                    </button>
                                                                    <button class="btn btn-danger no-padding i-delete-btn action-btn"
                                                                            data-id="<?= $childPermission[ 'id' ] ?>">
                                                                        <span class="fa fa-trash">删除</span>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        <?php else : ?>
                                                            <div class="col-xs-12">
                                                                <p class="text-muted well well-sm no-shadow"
                                                                   style="margin-top: 10px;">没有数据
                                                                </p>
                                                            </div>
                                                        <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="edit-admin">

    </div>
    <div class="modal fade" id="add-admin" tabindex="-1" role="dialog" aria-labelledby="addAdminModalLabel">
        <div class="modal-dialog" role="document" style="width:400px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addAdminModalLabel">添加权限</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>权限名称</label>
                        <input type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label>父级权限</label>
                        <br/>
                        <select aria-hidden="true" tabindex="-1" id="select-permission"
                                class="select2-hidden-accessible"
                                style="width: 100%;" name="parent">
                            <option value="" selected="selected">默认为一级权限</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>控制器名称</label>
                        <input type="text" class="form-control" id="controller-name" value="">
                    </div>
                    <div class="form-group">
                        <label>方法名称</label>
                        <input type="text" class="form-control" id="action-name" value="">
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                        <br/>
                        正常<input type="radio" name="status" checked value="1">
                        禁用<input type="radio" name="status" value="0">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="add">添加</button>
                </div>
                <?php
                $js = <<<JS
                jq(document).ready(function(){
                    jq.fn.modal.Constructor.prototype.enforceFocus = function () { };
                    //添加 start
                    jq("#add").click(function(){
                        var name = trim(jq("#name").val());
                        var controller = trim(jq("#controller-name").val());
                        var action = trim(jq("#action-name").val());
                        var parent = jq("#select-permission").val();
                        var status = jq("input[name=\"status\"]:checked").val();
                        if( name.length <= 0 ){
                            layer.msg("请填写名称",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return ;
                        }
                        if( name.length > 20 ){
                            layer.msg("名称长度不能超过20个字",{
                                icon : 2 ,
                                time : 2000 ,
                            });
                            return ;
                        }
                        var index = layer.load(1);
                        jq("#name,#controller-name,#action-name").val("");
                        jq.ajax({
                            url : "/rbac-permission/add-permission" ,
                            data : { status : status , name : name , controller : controller , action : action , parent : parent } ,
                            dataType : "json" ,
                            type : "POST" ,
                            success : function( data ){
                                layer.close(index);
                                if( data.code == "success" ){
                                    layer.msg("添加成功",{
                                        icon : 1 ,
                                        time : 1000 ,
                                    });
                                    setTimeout("window.location.reload()" , 1100);
                                }
                                if(data.code == "error" ){
                                    layer.msg( data.msg , {
                                        icon : 2 ,
                                        time : 2000 ,
                                    });
                                }
                            },//success end
                        });
                    });
                    //添加 end
                });
                //icheck start
                jq("input[name=\"status\"]").iCheck({
                    radioClass : "iradio_square-blue",
                });
                //icheck end
                //select2 start
                jq("#select-permission").select2({
                    ajax : {
                        url : "/rbac-permission/ajax-search-parent-permission" ,
                        dataType : "json" ,
                        type : "GET" ,
                        data : function( term , size ){
                            return {
                                search : term ,
                                size : 10 ,
                            };
                        },//data end
                        processResults : function( data ){
                            return {
                                results : data.msg ,
                            };
                        },//results end
                    },
                });
                //select2 end
JS;
                $this->registerJs( $js );
                ?>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
jq(document).ready(function(){
    //编辑 start
    jq(".i-edit-btn").on("click",function(){
        var id = jq(this).attr("data-id");
        layer.open({
            type : 2 ,
            title : "编辑" ,
            shadeClose : false ,
            shade : 0.6 ,
            area : ["300px","400px"],
            content : "/rbac-permission/edit-permission?id=" + id ,
        });
    });
    //编辑 end
    //单个删除权限 start
    jq(".i-delete-btn").on("click",function(){
        var id = jq(this).attr("data-id");
        var ids = [];
        ids.push( id );
        layer.msg("确定删除?",{
            btn : ["确定","取消"] ,
            time : 0 ,
            yes : function(index){
                layer.close(index);
                var _index = layer.load(1);
                jq.ajax({
                    url : "/rbac-permission/delete-permission" ,
                    data : { ids : ids } ,
                    dataType : "json" ,
                    type : "POST" ,
                    success : function(data){
                        layer.close(_index);
                        if( data.code == "success" ){
                            layer.msg("删除成功",{
                                icon : 1 ,
                                time : 1000 ,
                            });
                            setTimeout("window.location.reload()" , 1100);
                        }
                        if( data.code == "error" ){
                            layer.msg( data.msg , {
                                icon : 2 ,
                                time : 2000 ,
                            });
                        }
                    },//success end
                });
            },//yes end
        });
    });
    //单个删除权限 end
    //删除父级权限 start
    jq(".delete-parent-permission").on("click",function(){
        var id = jq(this).attr("data-id");
        layer.msg("连同子级权限一同删除.确定删除?",{
            btn : ["确定","取消"] ,
            time : 0 ,
            yes : function(index){
                layer.close(index);
                var _index = layer.load(1);
                jq.ajax({
                    url : "/rbac-permission/delete-parent-permission" ,
                    data : { id : id } ,
                    dataType : "json" ,
                    type : "POST" ,
                    success : function(data){
                        layer.close(_index);
                        if( data.code == "success" ){
                            layer.msg("删除成功",{
                                icon : 1 ,
                                time : 1000 ,
                            });
                            setTimeout("window.location.reload()" , 1100);
                        }
                        if( data.code == "error" ){
                            layer.msg( data.msg , {
                                icon : 2 ,
                                time : 1000 ,
                            });
                        }
                    },//success end
                });
            },//yes end
        });
    });
    //删除父级权限 end
    
    //批量删除 start
    jq(".delete-btn").on("click",function(){
        var id = jq(this).attr("data-id");
        var ids = [];
        jq("input[name=\"permission-checkbox-" + id + "\"]").each(function(){
            if(jq(this).prop("checked")){
                var id = jq(this).attr("data-id");
                ids.push( id );
            }
        });
        if( ids.length == 0 ){
            layer.msg("请选择要删除的权限",{
                icon : 0,
                time : 2000 ,
            });
            return ;
        }
        layer.msg("确定删除?",{
            btn : ["确定","取消"] ,
            time : 0 ,
            yes : function( index ){
                layer.close(index);
                var _index = layer.load(1);
                jq.ajax({
                    url : "/rbac-permission/delete-permission" ,
                    data : { ids : ids } ,
                    dataType : "json" ,
                    type : "POST" ,
                    success : function( data ){
                        layer.close(_index);
                        if( data.code == "success" ){
                            layer.msg("删除成功",{
                                icon : 1 ,
                                time : 1000 ,
                            });
                            setTimeout("window.location.reload()" , 1100);
                        }
                        if( data.code == "error" ){
                            layer.msg(data.msg , {
                                icon : 2 ,
                                time : 2000 ,
                            });
                        }
                    },//success end
                });
            },//yes end
        });
    });
    //批量删除 end
    //全选/反选 start
    jq(".parent-permission-check-all").on("ifChecked",function( event ){
        var id = jq(this).attr("data-id");
        jq("input[name=\"permission-checkbox-" + id + "\"]").iCheck("check");
    });
    jq(".parent-permission-check-all").on("ifUnchecked" , function( event ){
        var id = jq(this).attr("data-id");
        jq("input[name=\"permission-checkbox-" + id + "\"]").iCheck("uncheck");
    });
    //全选/反选 end
    //icheck 样式
    jq(".permission-check").iCheck({
        checkboxClass : "icheckbox_minimal-blue",
    });
    //icheck 样式
    //重置 start
    jq("#reset").click(function(){
        window.location.href = window.location.pathname;
    });
    //重置 end
    //填充搜索条件 start
    var searchStr = window.location.search;
    if( searchStr.length > 0 ){
        searchStr = decodeURI( searchStr.substr(1,searchStr.length) );
        searchStr = decodeURI( searchStr );
        //searchStr = searchStr.substr(1,searchStr.length);
        var searchArr = searchStr.split("&");
        for( var i = 0 ; i < searchArr.length ; i++ ){
            var _tmp = searchArr[i].split("=");
            jq("#" + _tmp[0]).val(_tmp[1]);
        }
    }
    //填充搜索条件 end
    //打开添加管理员页面 start
    jq("#open-add-page").click(function(){
        jq("#add-admin").modal({
            backdrop : false ,
        });
    });
    //打开添加管理员页面 end
    //搜索 start
    jq("#search").click(function(){
        var param = "";
        jq(".search-param").each(function(){
            var _str = jq(this).prop("id") + "=" + jq(this).val() + "&";
            param += jq(this).prop("id") + "=" + jq(this).val() + "&";
        });
        param = encodeURI( "?" + param.substr(0,param.length - 1) );
        param = encodeURI( param );
        //param = "?" + param.substr(0,param.length - 1);
        window.location.href = param;
   
    });
    //搜索 end
});
JS;
$this->registerJs( $js );
?>