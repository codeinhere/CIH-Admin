<?php
use backend\assets\AppAsset;

AppAsset::register( $this );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/select2.full.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerJsFile( '@web/css/adminlte/bower_components/select2/dist/js/i18n/zh-CN.js' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
$this->registerCssFile( '@web/css/adminlte/bower_components/select2/dist/css/select2.css' , [ AppAsset::className() , 'depends' => [ 'backend\assets\AppAsset' ] ] );
?>
<?php if ( empty( $permission ) ) : ?>
    <p>没有数据</p>
<?php else : ?>
    <div class="row">
        <input type="hidden" id="edit-id" value="<?= $permission[ 'id' ] ?>">
        <div class="col-xs-12">
            <div class="box-body">
                <div class="form">
                    <div class="form-group">
                        <label>权限名称</label>
                        <input type="text" id="permission_name" class="form-control"
                               value="<?= $permission[ 'permission_name' ] ?>">
                    </div>
                    <div class="form-group">
                        <label>控制器</label>
                        <input type="text" id="controller_name" value="<?= $permission[ 'route_controller' ] ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label>方法</label>
                        <input type="text" id="action_name" value="<?= $permission[ 'route_action' ] ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label>状态</label>
                        <br/>
                        <?php if ( $permission[ 'status' ] == 1 ) : ?>
                            正常<input type="radio" name="status" checked value="1">
                            禁用<input type="radio" name="status" value="0">
                        <?php else : ?>
                            正常<input type="radio" name="status" value="1">
                            禁用<input type="radio" name="status" checked value="0">
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-flat btn-sm" id="edit-save"> 保存</button>
                        <button type="button" class="btn btn-default btn-flat btn-sm" id="edit-cancel"> 取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $js = <<<JS
jq(document).ready(function(){
        //关闭 start
        jq("#edit-cancel").on("click",function(){
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        });
        //icheck start
        jq("input[name=\"status\"]").iCheck({
            radioClass : "iradio_square-blue",
        });
        //icheck end
        //关闭 end
        //保存 start
        jq("#edit-save").on("click",function(){
            var permissionName = trim(jq("#permission_name").val());
            var controllerName = trim(jq("#controller_name").val());
            var actionName = trim(jq("#action_name").val());
            var status = jq("input[name=\"status\"]:checked").val();
            var editId = jq("#edit-id").val();
            if( permissionName.length <= 0 ){
                layer.msg("请填写权限名称",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            if( permissionName.length > 20 ){
                layer.msg("权限名称不能超过20个字符",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            if( controllerName.length <= 0 ){
                layer.msg("请填写控制器名称",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            if( actionName.length <= 0 ){
                layer.msg("请填写方法名称",{
                    icon : 2 ,
                    time : 2000 ,
                });
                return ;
            }
            jq("#permissionName").val("");
            jq("#controllerName").val("");
            jq("#actionName").val("");
            var index = layer.load(1);
            jq.ajax({
                url : "edit-permission" ,
                data : { editId : editId , permissionName : permissionName , controllerName : controllerName , actionName : actionName , status : status } ,
                dataType : "json" ,
                type : "POST" ,
                success : function( data ){
                    layer.close(index);
                    if( data.code == "success" ){
                        layer.msg("修改成功",{
                            icon : 1 ,
                            time : 1000 ,
                        });
                        var _index = parent.layer.getFrameIndex(window.name);
                        setTimeout("parent.layer.close(" + index + ");parent.window.location.reload()" , 1100);
                    }
                    if( data.code == "error" ){
                        layer.msg( data.msg , {
                            icon : 2 ,
                            time : 2000 ,
                        });
                        return ;
                    }
                },//success end
            });
        });
        //保存 end
    });
JS;
    $this->registerJs( $js ); ?>
<?php endif; ?>