/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.44.142
 Source Server Type    : MySQL
 Source Server Version : 50556
 Source Host           : 192.168.44.142
 Source Database       : rbac_project

 Target Server Type    : MySQL
 Target Server Version : 50556
 File Encoding         : utf-8

 Date: 08/28/2018 11:15:11 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `backend_admin`
-- ----------------------------
DROP TABLE IF EXISTS `backend_admin`;
CREATE TABLE `backend_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(255) DEFAULT '' COMMENT '用户名',
  `password` varchar(255) DEFAULT '' COMMENT '密码',
  `register_time` int(11) DEFAULT '0' COMMENT '注册时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '账户状态0=禁用1=正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
--  Records of `backend_admin`
-- ----------------------------
BEGIN;
INSERT INTO `backend_admin` VALUES ('1', 'master', '$2y$10$7xENOlOk6o2oUzY4T4IG1.BW5CBANunZiURt2g3Msa3eB048rg9km', '1529335266', '1535207741', '1'), ('2', 'admin01', '$2y$10$GTIs18bMiWFa0W.bqmWfJOxlLxm7d1mLWk27cjHpmcvlI45m1BNhK', '1535219615', '1535219615', '1');
COMMIT;

-- ----------------------------
--  Table structure for `rbac_admin_permission`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_admin_permission`;
CREATE TABLE `rbac_admin_permission` (
  `admin_id` int(11) DEFAULT '0' COMMENT '管理员id',
  `permission_id` int(11) DEFAULT '0' COMMENT '权限id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限关联表';

-- ----------------------------
--  Table structure for `rbac_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_admin_role`;
CREATE TABLE `rbac_admin_role` (
  `role_id` int(11) DEFAULT '0' COMMENT '角色id',
  `admin_id` int(11) DEFAULT '0' COMMENT '管理员id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色管理员联系表';

-- ----------------------------
--  Table structure for `rbac_menu`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_menu`;
CREATE TABLE `rbac_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `menu_name` varchar(255) DEFAULT '' COMMENT '菜单名称',
  `visible` tinyint(1) DEFAULT '1' COMMENT '是否可见0=不可见1=可见',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级id',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态0=可用1=不可用',
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员id',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `permission_id` int(11) DEFAULT '0' COMMENT '权限id',
  `icon` varchar(255) DEFAULT '' COMMENT '图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
--  Table structure for `rbac_permission`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `permission_name` varchar(255) DEFAULT '' COMMENT '权限名称',
  `route_controller` varchar(255) DEFAULT '' COMMENT '控制器',
  `route_action` varchar(255) DEFAULT '' COMMENT '执行动作',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态0=禁用1=启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
--  Records of `rbac_permission`
-- ----------------------------
BEGIN;
INSERT INTO `rbac_permission` VALUES ('1', '管理员管理权限', '', '', '0', '1'), ('2', '所有管理员', 'backend-admin', 'admin', '1', '1'), ('3', '添加管理员', 'backend-admin', 'add-admin', '1', '1'), ('4', '异步请求', 'backend-admin', 'ajax-query-admin-page', '1', '1'), ('5', '切换状态', 'backend-admin', 'toggle-admin-status', '1', '1'), ('6', '删除管理员', 'backend-admin', 'delete-admin', '1', '1'), ('7', '编辑管理员', 'backend-admin', 'edit-admin', '1', '1'), ('8', '管理员详情', 'backend-admin', 'detail-admin', '1', '1'), ('9', '分配管理员权限', '', '', '0', '1'), ('10', '所有权限', 'rbac-admin-permission', 'set-admin-permission', '9', '1'), ('11', '编辑管理员权限', 'rbac-admin-permission', 'edit-admin-permission', '9', '1'), ('12', '菜单管理权限', '', '', '0', '1'), ('13', '所有菜单', 'rbac-menu', 'set-admin-menu', '12', '1'), ('14', '异步查询父级菜单', 'rbac-menu', 'ajax-search-parent-menu', '12', '1'), ('15', '删除菜单', 'rbac-menu', 'delete-menu', '12', '1'), ('16', '编辑菜单', 'rbac-menu', 'edit-menu', '12', '1'), ('17', '添加菜单', 'rbac-menu', 'add-admin-menu', '12', '1'), ('18', '编辑父级菜单', 'rbac-menu', 'edit-parent-menu', '12', '1'), ('19', '异步查询权限', 'rbac-menu', 'ajax-search-permission', '12', '1'), ('20', '权限管理', '', '', '0', '1'), ('21', '所有权限', 'rbac-permission', 'permission', '20', '1'), ('22', '添加权限', 'rbac-permission', 'add-permission', '20', '1'), ('23', '删除权限', 'rbac-permission', 'delete-permission', '20', '1'), ('24', '删除父级权限', 'rbac-permission', 'delete-parent-permission', '20', '1'), ('25', '异步查询权限', 'rbac-permission', 'ajax-search-permission', '20', '1'), ('26', '异步查询父级权限', 'rbac-permission', 'ajax-search-parent-permission', '20', '1'), ('27', '编辑权限', 'rbac-permission', 'edit-permission', '20', '1'), ('28', '角色管理', '', '', '0', '1'), ('29', '所有角色', 'rbac-role', 'role', '28', '1'), ('30', '添加角色', 'rbac-role', 'add-role', '28', '1'), ('31', '异步查询数据', 'rbac-role', 'ajax-role-query-page', '28', '1'), ('32', '切换角色状态', 'rbac-role', 'toggle-role-status', '28', '1'), ('33', '删除角色', 'rbac-role', 'delete-role', '28', '1'), ('34', '编辑角色', 'rbac-role', 'edit-role', '28', '1'), ('35', '分配角色权限', '', '', '0', '1'), ('36', '所有权限', 'rbac-role-permission', 'set-role-permission', '35', '1'), ('37', '编辑角色权限', 'rbac-role-permission', 'edit-role-permission', '35', '1');
COMMIT;

-- ----------------------------
--  Table structure for `rbac_permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission_role`;
CREATE TABLE `rbac_permission_role` (
  `permission_id` int(11) DEFAULT '0' COMMENT '权限id',
  `role_id` int(11) DEFAULT '0' COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限角色关联表';

-- ----------------------------
--  Table structure for `rbac_role`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_name` varchar(255) DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) DEFAULT '1' COMMENT '角色状态0=禁用1=启用',
  `create_time` int(11) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

SET FOREIGN_KEY_CHECKS = 1;
