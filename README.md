#Usage
1. 导入 ./rbac_project.sql
2. 装redis 和 yii-redis
3. 集成了 adminlte
4. 配置 redis 和 db 连接
5. 美化 url 配置 nginx 或者 apache 隐藏 index.php
6. 配置好菜单后注释掉 layouts/left-menu.php 的默认菜单代码
7. 已经默认添加了管理员账号 master 密码 123456
8. 默认已经把代码里面的权限添加到数据库了

**最后去掉该行注释**
<br/>
`//$this->authentication( $adminId );`

```
文件地址:backend/base/controller/BaseController.php
/**
* 初始化函数
*/
public function init()
{
    parent::init();
    $this->checkLogin();
    $adminId = Yii::$app->session->get( 'admin_id' , '' );
    //去掉下面的注释。让权限验证生效
    //$this->authentication( $adminId );
    $this->setRequestRouteMenu();
}
```
***
#Introduction

 - 路由指定只能是数字和大小写字母组成
 - 角色可以拥有权限
 - 独立的菜单表、管理员权限表
 - 角色可以拥有权限
 - 管理员可以同时拥有角色和权限
 
#Help

- 由于单独分离出的管理员可以同时拥有角色和权限，所以在配置权限的时候尽量不要重复。否则会造成在禁用角色的情况下，该角色下的权限要是分配给了管理员，验证不会生效

